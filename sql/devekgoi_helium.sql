-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 07, 2020 at 08:50 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `devekgoi_helium`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `hospital_id` bigint(20) NOT NULL,
  `test_id` text COLLATE utf8mb4_unicode_ci,
  `agent_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `booking_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `patient_category_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `identity_proof` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `identity_proof_No` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `report` text COLLATE utf8mb4_unicode_ci,
  `is_COPD` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'No',
  `is_chronic_renal_disease` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'No',
  `is_diabetes` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'No',
  `is_heart_disease` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'No',
  `is_bronshitis` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'No',
  `is_hypertension` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'No',
  `is_malignancy` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'No',
  `is_asthma` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'No',
  `hospitalization_date` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `diagnosis` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `differential_diagnosis` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `etiology_identified` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_no` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hospital_name_address` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_of_the_doctor` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `booking_date` timestamp NULL DEFAULT NULL,
  `payment_method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_amount` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_collected` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_deposited` tinyint(4) NOT NULL DEFAULT '0',
  `is_report_generated` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_report_collected` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `otp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `user_id`, `hospital_id`, `test_id`, `agent_id`, `booking_id`, `patient_category_id`, `dob`, `identity_proof`, `identity_proof_No`, `report`, `is_COPD`, `is_chronic_renal_disease`, `is_diabetes`, `is_heart_disease`, `is_bronshitis`, `is_hypertension`, `is_malignancy`, `is_asthma`, `hospitalization_date`, `diagnosis`, `differential_diagnosis`, `etiology_identified`, `phone_no`, `hospital_name_address`, `name_of_the_doctor`, `date`, `booking_date`, `payment_method`, `payment_type`, `payment_amount`, `is_collected`, `is_deposited`, `is_report_generated`, `is_report_collected`, `otp`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 22, '1', '14', '11111', NULL, NULL, NULL, NULL, NULL, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-10-09 12:17:53', NULL, NULL, NULL, NULL, 1, NULL, NULL, '123206', '2020-10-09 12:01:33', '2020-10-27 20:20:45', NULL),
(2, 3, 22, '1', '15', '22222', NULL, NULL, NULL, NULL, NULL, 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-10-23 11:48:33', NULL, NULL, NULL, NULL, 0, NULL, NULL, '545052', '2020-10-09 05:00:36', '2020-11-04 09:17:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bookingpivot`
--

CREATE TABLE `bookingpivot` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `hospital_id` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bookingpivot`
--

INSERT INTO `bookingpivot` (`id`, `user_id`, `hospital_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 4, 9, NULL, NULL, NULL),
(2, 3, 9, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `city_name` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `country_name` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hospital`
--

CREATE TABLE `hospital` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `hospital_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `license` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registration_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `pin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_verified` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_deactivate` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hospital`
--

INSERT INTO `hospital` (`id`, `user_id`, `hospital_name`, `license`, `registration_number`, `address`, `pin`, `location`, `country_id`, `state_id`, `city_id`, `is_verified`, `is_deactivate`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 9, 'GNRC', NULL, 'red123', 'Aaddddd. fjkjfkjf', '08820', NULL, NULL, NULL, NULL, NULL, 0, '2020-10-10 01:17:27', '2020-11-02 12:05:15', NULL),
(2, 10, 'Pratiksha', NULL, NULL, 'odalbakra, Lal Ganesh, Guwahati', '781034', NULL, NULL, NULL, NULL, NULL, 1, '2020-10-09 01:40:56', '2020-10-28 02:13:56', '2020-10-28 02:13:56'),
(3, 13, 'Neemcare', NULL, '12345678', 'odalbakra, Lal Ganesh, Guwahati', '781034', NULL, NULL, NULL, NULL, NULL, 0, '2020-10-14 01:01:55', '2020-10-16 23:18:00', '2020-10-16 23:18:00'),
(4, 16, 'Neemacre', NULL, NULL, 'odalbakra, Lal Ganesh, delhi', '7772222', NULL, NULL, NULL, NULL, NULL, 0, '2020-10-19 03:11:17', '2020-10-19 03:16:36', '2020-10-19 03:16:36'),
(5, 17, 'pratik', NULL, '12378', 'kjjhjikj', '7657654', NULL, NULL, NULL, NULL, NULL, 0, '2020-10-22 02:59:45', '2020-10-22 03:27:06', '2020-10-22 03:27:06'),
(6, 18, 'kjhkj', NULL, 'huwq3e123', 'hujiu', 'iuohoi', NULL, NULL, NULL, NULL, NULL, 0, '2020-10-22 03:02:17', '2020-10-22 03:26:15', '2020-10-22 03:26:15'),
(7, 21, 'Nemcare', NULL, '123', 'odalbakra, Lal Ganesh, Guwahati', '781034', NULL, NULL, NULL, NULL, NULL, 0, '2020-10-22 12:34:46', '2020-10-30 07:12:09', NULL),
(8, 22, 'GMCH', NULL, '1232545', 'odalbakra, Lal Ganesh, Guwahati', '781034', NULL, NULL, NULL, NULL, NULL, 0, '2020-10-27 07:23:24', '2020-10-27 07:23:24', NULL),
(9, 23, 'a', NULL, 'a', 'Ganeshguri, Kahilipara Road', '781019', NULL, NULL, NULL, NULL, NULL, 0, '2020-10-29 10:54:40', '2020-10-29 10:55:26', '2020-10-29 10:55:26'),
(10, 24, 'd', NULL, 'dd', 'dd', 'dd', NULL, NULL, NULL, NULL, NULL, 0, '2020-10-29 11:02:06', '2020-10-29 11:04:56', '2020-10-29 11:04:56'),
(11, 25, 'd', NULL, NULL, 'dd', '781', NULL, NULL, NULL, NULL, NULL, 0, '2020-10-29 11:02:29', '2020-10-29 11:10:36', '2020-10-29 11:10:36'),
(12, 26, 'abcd', NULL, 'abcd', 'abcd', 'abcd', NULL, NULL, NULL, NULL, NULL, 0, '2020-10-29 11:12:07', '2020-10-29 12:15:42', '2020-10-29 12:15:42'),
(13, 27, 'ffh', NULL, 'hh', NULL, '1234512', NULL, NULL, NULL, NULL, NULL, 0, '2020-10-31 06:58:19', '2020-10-31 06:59:17', '2020-10-31 06:59:17'),
(14, 28, 'Hayat', NULL, 'abcd', 'Ganeshguri, Kahilipara Road', '781019', NULL, NULL, NULL, NULL, NULL, 0, '2020-11-03 06:56:44', '2020-11-03 07:16:38', NULL),
(15, 29, 'dsffdsfd', NULL, 'dd', NULL, '123453', NULL, NULL, NULL, NULL, NULL, 0, '2020-11-03 07:18:12', '2020-11-03 07:18:25', '2020-11-03 07:18:25');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(53, '2014_10_12_000000_create_users_table', 1),
(54, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(55, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(56, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(57, '2016_06_01_000004_create_oauth_clients_table', 1),
(58, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(59, '2019_08_19_000000_create_failed_jobs_table', 1),
(60, '2020_08_23_083052_create_users_details_table', 1),
(61, '2020_10_06_093831_create_roll_table', 1),
(62, '2020_10_07_012234_create_hospital_table', 1),
(64, '2020_10_07_122157_create_test_table', 1),
(65, '2020_10_07_112816_create_booking_table', 2),
(66, '2020_10_08_120912_create_todos_table', 2),
(67, '2020_10_10_071954_create_bookinpivot_table', 3),
(70, '2014_10_12_100000_create_password_resets_table', 4),
(71, '2020_10_15_070627_add__unique_to_users_table', 5),
(72, '2020_10_20_072135_add_deatils_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `notifiable_type`, `notifiable_id`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
('136852dd-176f-47ce-aae2-c4d9ce320fa5', 'App\\Notifications\\UserRegistration', 'App\\User', 1, '{\"data\":\"user Register\"}', NULL, '2020-10-28 23:51:09', '2020-10-28 23:51:09'),
('31ecdee9-262b-4331-bf6d-9a4f829d1b91', 'App\\Notifications\\UserRegistration', 'App\\User', 1, '{\"data\":\"hi how are you\"}', NULL, '2020-10-29 01:00:16', '2020-10-29 01:00:16'),
('4403c953-82c5-4d31-8ad2-28da44652515', 'App\\Notifications\\UserRegistration', 'App\\User', 12, '{\"name\":1,\"details\":\"pranjal\"}', NULL, '2020-10-29 06:00:16', '2020-10-29 06:00:16'),
('558d7e40-c728-4d1d-99ae-c88d4942a6c9', 'App\\Notifications\\UserRegistration', 'App\\User', 1, '{\"data\":\"okpok\"}', NULL, '2020-10-29 00:04:30', '2020-10-29 00:04:30'),
('8a4d4da1-f8e7-4b69-9f94-0768ac468a6d', 'App\\Notifications\\UserRegistration', 'App\\User', 1, '{\"data\":\"user Register\"}', NULL, '2020-10-28 23:13:29', '2020-10-28 23:13:29'),
('b7748083-762b-4072-9551-5eb25096dd2c', 'App\\Notifications\\UserRegistration', 'App\\User', 1, '{\"data\":\"hi how are you\"}', NULL, '2020-10-29 00:05:08', '2020-10-29 00:05:08'),
('d11fd277-4928-42bc-a47c-b3c6a98bd1bd', 'App\\Notifications\\UserRegistration', 'App\\User', 12, '{\"name\":\"hi how are you\",\"details\":\"pranjal\"}', NULL, '2020-10-29 05:54:15', '2020-10-29 05:54:15');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('0a360b4b3cced78cab50d242906f1653425b52bc01e98c20ea656340bac4402eac0cbe3367e6de87', 2, 2, 'MyApp', '[]', 0, '2020-10-08 01:55:58', '2020-10-08 01:55:58', '2021-04-08 07:25:57'),
('1849b3170e5ea94e9c8ef935239b6399f4c425eda0187f5b9e8947fafc50373b38bcb96c1b27c1ff', 9, 2, 'MyApp', '[]', 0, '2020-10-12 03:26:58', '2020-10-12 03:26:58', '2020-10-12 09:01:58'),
('2c1eb75fb31fefc18cc70a887995583489f8766043793fe2721a070c57e14d2bba452fad026e8f2b', 8, 2, 'MyApp', '[]', 0, '2020-10-08 02:47:39', '2020-10-08 02:47:39', '2020-10-08 10:17:38'),
('2e2c8a624f9ac9345288e3a2b015a21c26133305f087c1d8615ce6de1ec2044a51ee63f93d9238cd', 9, 2, 'MyApp', '[]', 0, '2020-10-12 01:39:19', '2020-10-12 01:39:19', '2020-10-12 07:14:19'),
('364be203e2a7b8eb4da4aa2ef6bb5cf87299258e8083524498b7aa7a1cd2d5d9636dda4590973283', 5, 2, 'MyApp', '[]', 0, '2020-10-08 02:04:27', '2020-10-08 02:04:27', '2020-10-08 08:34:27'),
('385c15cc266282768bd703e688fa7cff80364bd995db0b3849738e644b7f02814344ba4fdf152377', 22, 2, 'MyApp', '[]', 0, '2020-10-27 07:23:24', '2020-10-27 07:23:24', '2020-10-27 07:28:24'),
('3a6910f4543384e42ba1a3f8758a7275e57047fe7d79b604852af80d34a5e28814552daa061145ed', 9, 2, 'MyApp', '[]', 0, '2020-10-12 03:38:26', '2020-10-12 03:38:26', '2020-10-12 09:13:26'),
('49ff39d28fe4fe41c0a1fcc092a2ccbdf93c280d1b979db26d2dfa71ea14ea606c0324eb8eee4b27', 9, 2, 'MyApp', '[]', 0, '2020-10-10 01:55:40', '2020-10-10 01:55:40', '2020-10-10 09:25:40'),
('4db1ba6c85b0c20df831419cf30272eda8f27dbc8696041a9b45f892e2741c6bad08dfec03ed2e5e', 23, 2, 'MyApp', '[]', 0, '2020-10-29 10:54:40', '2020-10-29 10:54:40', '2020-10-29 10:59:39'),
('510476e21ab9c1a2814d99b0e7dbd42715fda229b1c593437a2dbddea5a5bf2d0999c3d34438bf7a', 3, 2, 'MyApp', '[]', 0, '2020-10-08 02:00:49', '2020-10-08 02:00:49', '2021-04-08 07:30:49'),
('5c20f5209ee0d7c0b7e34621da9870fdabbe3878eae221cb3d33fdd59b48a980c66781bec028bb1a', 8, 2, 'MyApp', '[]', 0, '2020-10-08 02:47:39', '2020-10-08 02:47:39', '2020-10-08 10:17:38'),
('5e1047344139d9afe45456b596d0ce88d18a2afdbd6f4746833a355b644c4f8c152b23be871ddd31', 9, 2, 'MyApp', '[]', 0, '2020-10-09 23:55:29', '2020-10-09 23:55:29', '2020-10-10 07:25:29'),
('61641cd30481b8dc92d7d08daa94d62c656ffd20333a54ed525844d4c46f7fb93ba09b12aae8a7b9', 26, 2, 'MyApp', '[]', 0, '2020-10-29 11:12:07', '2020-10-29 11:12:07', '2020-10-29 11:17:07'),
('66497910c8a5b7461840e7865467ef7dab1b5fab502910b42ddd1d915fe3140a10caa7665dab9d6a', 9, 2, 'MyApp', '[]', 0, '2020-10-12 01:19:33', '2020-10-12 01:19:33', '2020-10-12 08:49:31'),
('6c92061f9a510425c7910e39d29e97f76040f5cdc4b13cdb14d811787efa2cfd23ce71561d01dd69', 7, 2, 'MyApp', '[]', 0, '2020-10-08 02:44:18', '2020-10-08 02:44:18', '2020-10-08 10:14:18'),
('6f9083ec212fb92012c4be003667771fba4aac322287a010632b19e5c233477e0f5e4768bff7f9af', 9, 2, 'MyApp', '[]', 0, '2020-10-12 03:12:09', '2020-10-12 03:12:09', '2020-10-12 08:47:07'),
('7e31d515d6971d1f63a8fecbc27ec154a4f5f75941b885482ad7103f62fd1d42b4e32ce1449cfc6b', 28, 2, 'MyApp', '[]', 0, '2020-11-03 06:56:44', '2020-11-03 06:56:44', '2020-11-03 07:01:43'),
('8716e75f52e6f09119ca7e23446821ea99d2b746ba5dc5e3bd4ec46d33ca3c9aa8db1b2fd5712793', 9, 2, 'MyApp', '[]', 0, '2020-10-11 23:12:50', '2020-10-11 23:12:50', '2020-10-12 06:42:46'),
('89009d7f4c0269285625297f743dd312b9f4912c55ae643a489a7ce391140f90efb216ac2b070b1b', 9, 2, 'MyApp', '[]', 0, '2020-10-09 23:55:08', '2020-10-09 23:55:08', '2020-10-10 07:25:07'),
('8bd7847aeea76a26f04124fc55d436eb75936321945850850fd1e71dcbb44c9fd3f67ac048dd51fe', 9, 2, 'MyApp', '[]', 0, '2020-10-12 01:39:19', '2020-10-12 01:39:19', '2020-10-12 07:14:19'),
('8f6386ee98e5569672823cf4ca2002d75889c06402d3762ec71fdfee0c39ee28eca2bcfb1bf9f4bd', 9, 2, 'MyApp', '[]', 0, '2020-10-09 01:24:39', '2020-10-09 01:24:39', '2020-10-09 08:54:38'),
('8ffccfe648a36199bbb65534d3daa01d519630f068191f1564d4e3f8b266723be8ebb0a5f9681f52', 9, 2, 'MyApp', '[]', 0, '2020-10-12 01:55:19', '2020-10-12 01:55:19', '2020-10-12 07:30:18'),
('915b766dd88a38bc41736c3661d810b103045a7777fa531ea4c7a0eb4268e641e1a2002e14e71ba0', 25, 2, 'MyApp', '[]', 0, '2020-10-29 11:02:29', '2020-10-29 11:02:29', '2020-10-29 11:07:29'),
('9c9f1315f86571c5cd013657cb1b582a5ccb402b641be5aef361155639a720bfa89e4ec64abb8437', 9, 2, 'MyApp', '[]', 0, '2020-10-09 01:17:26', '2020-10-09 01:17:26', '2020-10-09 08:47:23'),
('a0588113f9f47e3ad5919fecf8161d3c6b43bd44fc43ee62ca0c4432d78cb26403ffb92f96598ad7', 29, 2, 'MyApp', '[]', 0, '2020-11-03 07:18:12', '2020-11-03 07:18:12', '2020-11-03 07:23:12'),
('a21901e9526890c6e8f086b5409025436211958d928714efe82d08d6dab1a9f2e3ba1b04fd021343', 9, 2, 'MyApp', '[]', 0, '2020-10-12 01:38:56', '2020-10-12 01:38:56', '2020-10-12 07:13:55'),
('a5168d4c8d77369a8c7b639d2bde83ed5b4e7224b887654d12510f7cdf435180199a34fa90f1426f', 17, 2, 'MyApp', '[]', 0, '2020-10-22 02:59:44', '2020-10-22 02:59:44', '2020-10-22 08:34:43'),
('a99f20ed774ca9ee3890af5281cb4daf9d08595d5fa2635425b9b3e7bcbd59bed5d271fccdeaaa2b', 24, 2, 'MyApp', '[]', 0, '2020-10-29 11:02:06', '2020-10-29 11:02:06', '2020-10-29 11:07:06'),
('ad38efe292489935131eb60e4a325bc03d2a81752a87483d5df9aa89176446b1a8c50b6ebbfd23b1', 9, 2, 'MyApp', '[]', 0, '2020-10-10 03:56:25', '2020-10-10 03:56:25', '2020-10-10 11:26:24'),
('ae59d4e8eeccfca4cf0ff132b3f4b39959bf85992eb542a1f720f09118ba423189d8d1352c04e29d', 8, 2, 'MyApp', '[]', 0, '2020-10-08 23:40:03', '2020-10-08 23:40:03', '2020-10-09 07:10:02'),
('b046f11b7c416a5c2031a73348015c21999914b2f05683f6842d28909a03473643610b926cf56bcb', 4, 2, 'MyApp', '[]', 0, '2020-10-08 02:03:26', '2020-10-08 02:03:26', '2020-10-08 08:33:26'),
('b940ce882a7c6771bec779e974ee3a025e5ba4df136a6fe72a3a667bf6ba33141e04c26048f3782c', 27, 2, 'MyApp', '[]', 0, '2020-10-31 06:58:19', '2020-10-31 06:58:19', '2020-10-31 07:03:19'),
('bdc2ba6af91299668cd8a01f29d3490af276b88bd83e7518f09ca85ec30ca7bd0a1915f8a4e148f1', 9, 2, 'MyApp', '[]', 0, '2020-10-12 01:24:15', '2020-10-12 01:24:15', '2020-10-12 08:54:15'),
('bdd5ee4ae75a303d6933d1b9279e4239be246404967bfb276876fc8085bcb755928e4d51332651e6', 9, 2, 'MyApp', '[]', 0, '2020-10-09 05:14:18', '2020-10-09 05:14:18', '2020-10-09 12:44:17'),
('c3e9b0948d7e97cc2247bc3dfe7dcdc62e101eb178f13dfd99558af96c07b7ee25bdf9020324c098', 21, 2, 'MyApp', '[]', 0, '2020-10-22 12:34:46', '2020-10-22 12:34:46', '2020-10-22 12:39:45'),
('c5a7ffd79045b872bb34c97090ef1501feac90e4316f784f24eb549464d5b46275a906d7322a8de4', 9, 2, 'MyApp', '[]', 0, '2020-10-09 01:21:43', '2020-10-09 01:21:43', '2020-10-09 08:51:43'),
('ceaca0ecfa96afc93df07df7c29db3d41224323a1ead3d404ed96955147b935e00d6664991dafecd', 13, 2, 'MyApp', '[]', 0, '2020-10-14 01:01:53', '2020-10-14 01:01:53', '2020-10-14 06:36:51'),
('d70554368973ecaad38c10cdbd295f05844e92815b3a4182f7a8edae1ca8a5e7c2a396ac97ae4dcc', 10, 2, 'MyApp', '[]', 0, '2020-10-09 01:40:56', '2020-10-09 01:40:56', '2020-10-09 09:10:56'),
('de6a11d4c7e53bb4b857e8044a2b1427f3847de4636b327782d9344b31397928b482db1c18a24749', 9, 2, 'MyApp', '[]', 0, '2020-10-12 01:38:55', '2020-10-12 01:38:55', '2020-10-12 07:13:55'),
('e3a77d5cbd63ae9984f36316e69251feb40fd7ea8a135fff77213b343ec33c99b3ab9b70196acd4d', 18, 2, 'MyApp', '[]', 0, '2020-10-22 03:02:17', '2020-10-22 03:02:17', '2020-10-22 08:37:17'),
('e5660710fe183acbe3fa34a23e8ec76dc5016615484adc678e662b5450d87787246ef3fec6343ffe', 16, 2, 'MyApp', '[]', 0, '2020-10-19 03:11:16', '2020-10-19 03:11:16', '2020-10-19 08:46:14'),
('e7843f63b67147fded3c83f5a7706c9a768404f88e031e5d2829a02529029a3293a9f78ee37f96f2', 6, 2, 'MyApp', '[]', 0, '2020-10-08 02:43:45', '2020-10-08 02:43:45', '2020-10-08 09:13:44'),
('ebb931f7726643e42891b19eab552d7b5b7b1f67cd1f19057736d3fdabcec7ce1515ab017acc3151', 9, 2, 'MyApp', '[]', 0, '2020-10-12 01:55:19', '2020-10-12 01:55:19', '2020-10-12 07:30:18'),
('f0262969aa093f8c22f8f00cadf76c6de106454a5963e9c7b6582087e05f312f884f514a15441303', 9, 2, 'MyApp', '[]', 0, '2020-10-13 01:45:02', '2020-10-13 01:45:02', '2020-10-13 07:19:58'),
('f685205c5c71cec06c674ee028841507368fddac0fd812244590d4d4e6af138ab6f700b1d53ea5cd', 9, 2, 'MyApp', '[]', 0, '2020-10-12 03:26:29', '2020-10-12 03:26:29', '2020-10-12 09:01:29');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Password Grant Client', 'EsefgucFpjKczjc8AMde9W6302HUHU5b5dxQ5Onx', 'users', 'http://localhost', 0, 1, 0, '2020-10-08 01:54:44', '2020-10-08 01:54:44'),
(2, NULL, 'Laravel Personal Access Client', 'WskzS5CROgHuzJm68IGIv5KfroTf5erA5uzWQ2mY', NULL, 'http://localhost', 1, 0, 0, '2020-10-08 01:55:37', '2020-10-08 01:55:37'),
(3, NULL, 'Laravel Password Grant Client', '0qJtAv2FjrUbcz2resFEFn0iVH9w24sxCm0fdzYo', 'users', 'http://localhost', 0, 1, 0, '2020-10-08 01:55:37', '2020-10-08 01:55:37');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 2, '2020-10-08 01:55:37', '2020-10-08 01:55:37');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `patient_details`
--

CREATE TABLE `patient_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `booking_id` bigint(20) NOT NULL,
  `is_foreign_country` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foreign_country_place` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_contact_with_lab` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `place_contact_lab` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_quarantined` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `where_quarantined` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_cough` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_vomit` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_breathlessness` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_haemoptysis` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_soar_throat` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_nasal_discharge` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_sputum` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_fever_at_evaluation` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_diarrhoea` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_chest_pain` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_abdominal_pain` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_nausea` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_body_ache` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `roll_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `roll_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', '2020-10-16 00:27:57', '2020-10-16 00:27:57', NULL),
(2, 'Hospital', '2020-10-16 00:28:06', '2020-10-16 00:28:06', NULL),
(3, 'Users', '2020-10-16 00:28:17', '2020-10-16 00:28:17', NULL),
(4, 'Agent', '2020-10-16 00:28:24', '2020-10-16 00:28:24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `id` int(11) NOT NULL,
  `state_name` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `testlist`
--

CREATE TABLE `testlist` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `test_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `todos`
--

CREATE TABLE `todos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unique_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_photo` text COLLATE utf8mb4_unicode_ci,
  `phone_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_deactivate` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `unique_id`, `role_id`, `email`, `profile_photo`, `phone_no`, `email_verified_at`, `password`, `is_deactivate`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'pranjal', NULL, 1, 'pranjal@gmail.com', NULL, NULL, NULL, '$2y$10$OG7VufxFJmIWFh08fUlBLOeDuw3YM6XgwIpAyigeuLwgzbqvA5USC', 0, NULL, '2020-10-08 01:55:30', '2020-10-08 01:55:30', NULL),
(2, 'pranjal', NULL, 1, 'pranja@gmail.com', NULL, NULL, NULL, '$2y$10$A913SoOVyB3VVy.VTHb7huP7jgYpwT87aLMpzzx24u0FQnuSk10L6', 0, NULL, '2020-10-08 01:55:58', '2020-10-08 01:55:58', NULL),
(3, 'manik', NULL, 3, 'pranja@mail.com', NULL, '8765432190', NULL, '$2y$10$1vD1s0M3uFBvkxU7bERKQOGi2UrIZYjfIxlY6p8iCybZA9Zq75SBW', 1, NULL, '2020-10-08 02:00:49', '2020-10-30 06:30:50', NULL),
(4, 'laal', NULL, 3, 'pranja@hail.com', NULL, NULL, NULL, '$2y$10$AoEYBJGewBgdKmmatIHfaOqK0QQIqBC3NYanKU/G7.urTi4HGV02y', 1, NULL, '2020-10-08 02:03:26', '2020-10-30 08:50:56', NULL),
(5, 'pranjal', NULL, 1, 'pranja@ail.com', NULL, NULL, NULL, '$2y$10$mTDZaG4/at2/Iw5zblpdi.tR0WOHbrG6PfLA2.o0CH7nDYWvcVFS.', 0, NULL, '2020-10-08 02:04:27', '2020-10-08 02:04:27', NULL),
(6, 'pranjal', NULL, 1, 'pranja@ail.cm', NULL, NULL, NULL, '$2y$10$.HeAL1RF2XCj.AMvabMuZ.mkMMpeyquJDgrKcAtGgv.R4vI0ECI5m', 0, NULL, '2020-10-08 02:43:45', '2020-10-08 02:43:45', NULL),
(7, 'pranjal', NULL, 1, 'pranja@il.cm', NULL, NULL, NULL, '$2y$10$kRXuHEeuWwerrhAZ2rmWHedmNXqFNoU7oEQS9LkJM2/5/uAUCj7Sq', 0, NULL, '2020-10-08 02:44:18', '2020-10-08 02:44:18', NULL),
(8, 'pranjal', NULL, 1, 'pranja@i.cm', NULL, NULL, NULL, '$2y$10$5LbKCWyBnaYiO2mjsnLMUO8w898Kqn.q6S/pmSyF/LDTHoPWGIwrC', 0, NULL, '2020-10-08 02:47:39', '2020-10-08 02:47:39', NULL),
(9, 'GNRC', NULL, 2, NULL, NULL, '8521458965', NULL, '$2y$10$HAGOlNW2GUUBar16dgMmquh6ztWcWIvOfMVxNP4DvjcyITsDPyLUe', 0, NULL, '2020-10-09 01:17:22', '2020-11-02 12:05:27', NULL),
(10, 'Pratiksha', NULL, 2, 'pranjaldeb360@gmail.com', NULL, '1234567', NULL, '$2y$10$vVn.DJ0u5pVJOLcqpPdsbe7P0DMx1c3jbpPLwrysATm4eK9/ehIDm', 0, NULL, '2020-10-09 01:40:56', '2020-10-28 02:13:56', '2020-10-28 02:13:56'),
(11, 'Neil', NULL, NULL, 'execute360@gmail.com', NULL, NULL, NULL, '$2y$10$XCoWzlqCSm6DTbGldXP5XOUGNN4ojRCnjj47lok6QLwav3IF4SMIS', 0, NULL, '2020-10-13 06:13:41', '2020-10-13 06:13:41', NULL),
(12, 'exe360', NULL, 1, 'karmahitsback@gmail.com', NULL, NULL, NULL, '$2y$10$kmOB4MNxLG9t2RirKmx1suoz8bgQViqbfLzP4.PFidlwrf5.K8dgi', 0, NULL, '2020-10-13 06:54:22', '2020-10-16 03:52:35', NULL),
(14, 'test', 'Q3CFiHkeD', 4, 'test@gmail.com', NULL, '1234567890', NULL, NULL, 1, NULL, '2020-10-15 01:49:51', '2020-11-03 10:55:33', NULL),
(15, 'Jishnu Thapa', '82275', 4, 'jishnu@gmail.com', '', '9876543210', NULL, NULL, 1, NULL, '2020-10-15 01:51:16', '2020-11-03 06:48:11', NULL),
(16, 'Neemacre', NULL, 2, 'ppppp@gmail.com', NULL, '9876543321', NULL, '$2y$10$D7XsvhML1hVvA/eZOWklX.4WOQ18.EpwDmPqukDT8BY2Tb3LVN.we', 1, NULL, '2020-10-19 03:11:13', '2020-10-19 03:16:36', '2020-10-19 03:16:36'),
(17, 'pratik', NULL, 2, 'pratik@gmail.com', NULL, '6548', NULL, '$2y$10$2I8Yk0eO9irOViY1BbQ2m.lzEct.i3ruItG1Dmb5F0dy5u.E6ym8y', 0, NULL, '2020-10-22 02:59:43', '2020-10-22 03:27:06', '2020-10-22 03:27:06'),
(18, 'kjhkj', NULL, 2, 'Warning', NULL, 'kjnbkjn', NULL, '$2y$10$/3ZEqwckHEN3BZCroqynMu2.DQfP5deWmFT2A5xRVN.COdJXlD.dK', 0, NULL, '2020-10-22 03:02:17', '2020-10-22 03:26:14', '2020-10-22 03:26:14'),
(19, 'oiyuoiu', '13869', 4, 'undercoverexe360@gmail.com', NULL, '1231', NULL, NULL, 0, NULL, '2020-10-22 03:45:41', '2020-10-22 03:46:07', '2020-10-22 03:46:07'),
(20, 'ewqrfwe', '50421', 4, 'undercoverexe360@gmail.com', NULL, 'wer', NULL, NULL, 0, NULL, '2020-10-22 03:46:32', '2020-10-22 03:46:58', '2020-10-22 03:46:58'),
(21, 'Nemcare', NULL, 2, 'pranjaldeb360@gmail.com', NULL, '08486463', NULL, '$2y$10$8wvQYGnlglR0hAgNUKhGhOmJzT8hxbOrnmbBOBFQ3iyfY3WpM4OtO', 1, NULL, '2020-10-22 12:34:46', '2020-11-02 12:04:49', NULL),
(22, 'GMCH', NULL, 2, 'pranjaldeb360@outlook.com', NULL, '8547854788', NULL, '$2y$10$yX0DpTSVDOLx55nChLHq/uX.KyOLMBm6Yx/Uv4/aoMiB/bt/I20VO', 1, NULL, '2020-10-27 07:23:24', '2020-10-31 06:49:24', NULL),
(23, 'a', NULL, 2, 'rajdeep.das@ekodus.com', NULL, '0', NULL, '$2y$10$j2i2jjnQ2EkuGA3T7b.ORui/9BKV3OxjoyjtOuP/cI3PzDMTOkAzW', 0, NULL, '2020-10-29 10:54:39', '2020-10-29 10:55:26', '2020-10-29 10:55:26'),
(24, 'd', NULL, 2, 'dd', NULL, 'dd', NULL, '$2y$10$qB0TlFNAKNC0bf/5NFT11Oahi41Mk/091Ukm4ifeghwjNsX7Ht1F.', 0, NULL, '2020-10-29 11:02:06', '2020-10-29 11:04:56', '2020-10-29 11:04:56'),
(25, 'd', NULL, 2, NULL, NULL, '88', NULL, '$2y$10$gT4LqDrbWMIIDCgBAc.nbuu7WkCrrlOViwssQmE1rG09s.m87Icwy', 0, NULL, '2020-10-29 11:02:29', '2020-10-29 11:10:36', '2020-10-29 11:10:36'),
(26, 'abcd', NULL, 2, 'abcd', NULL, 'abcd', NULL, '$2y$10$S0tOqI.XjSnluC92rOx28.4o7NXYbFy09mpk6AGe48NAcDguurmxq', 0, NULL, '2020-10-29 11:12:07', '2020-10-29 12:15:42', '2020-10-29 12:15:42'),
(27, 'ffh', NULL, 2, 'hydhgd@jksdkjds.com', NULL, '99999999999999999999999999999999999999999999988888888888888', NULL, '$2y$10$OthRo5C6JZL7GPXtx/oYIOQ7H6Ca8KTF3NwKMaJ6tt/AUBvPgc5xi', 0, NULL, '2020-10-31 06:58:19', '2020-10-31 06:59:17', '2020-10-31 06:59:17'),
(28, 'Hayat', NULL, 2, 'jondoe123@yahoo.com', NULL, '8547854785', NULL, '$2y$10$u2Mk55vkBXYL8yxNMr.b5eyz6vKLXUN3jnmaHzafCnYzOpRly9kom', 0, NULL, '2020-11-03 06:56:43', '2020-11-03 07:17:13', NULL),
(29, 'dsffdsfd', NULL, 2, 'dummyemail@email.com', NULL, '7788778878', NULL, '$2y$10$8QqjCRIAwXmU69J.dQitU.WnD43pmLjaPAPPfirz9Fo5AeTDk/l/W', 0, NULL, '2020-11-03 07:18:12', '2020-11-03 07:18:25', '2020-11-03 07:18:25');

-- --------------------------------------------------------

--
-- Table structure for table `usersdetails`
--

CREATE TABLE `usersdetails` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `age` timestamp NULL DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `pin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_deactivate` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `usersdetails`
--

INSERT INTO `usersdetails` (`id`, `user_id`, `age`, `gender`, `address`, `pin`, `location`, `country_id`, `state_id`, `city_id`, `is_deactivate`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, '2012-06-01 08:36:45', 'Male', 'jkbkj', NULL, NULL, NULL, NULL, NULL, '0', '2020-10-08 01:55:58', '2020-10-08 01:55:58', NULL),
(2, 4, '2012-06-01 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '2020-10-08 02:04:28', '2020-10-08 02:04:28', NULL),
(3, 6, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '2020-10-08 02:43:45', '2020-10-08 02:43:45', NULL),
(4, 7, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '2020-10-08 02:44:18', '2020-10-08 02:44:18', NULL),
(5, 8, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '2020-10-08 02:47:39', '2020-10-08 02:47:39', NULL),
(6, 14, NULL, NULL, 'oiuhiu', NULL, NULL, NULL, NULL, NULL, '0', '2020-10-15 01:49:51', '2020-11-03 10:55:33', NULL),
(7, 15, NULL, NULL, 'Sikkim', NULL, NULL, NULL, NULL, NULL, '0', '2020-10-15 01:51:16', '2020-11-03 04:29:12', NULL),
(8, 19, NULL, NULL, 'werw', NULL, NULL, NULL, NULL, NULL, '0', '2020-10-22 03:45:41', '2020-10-22 03:46:07', '2020-10-22 03:46:07'),
(9, 20, NULL, NULL, 'werw', NULL, NULL, NULL, NULL, NULL, '0', '2020-10-22 03:46:32', '2020-10-22 03:46:58', '2020-10-22 03:46:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookingpivot`
--
ALTER TABLE `bookingpivot`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hospital`
--
ALTER TABLE `hospital`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `patient_details`
--
ALTER TABLE `patient_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testlist`
--
ALTER TABLE `testlist`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `todos`
--
ALTER TABLE `todos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usersdetails`
--
ALTER TABLE `usersdetails`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bookingpivot`
--
ALTER TABLE `bookingpivot`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hospital`
--
ALTER TABLE `hospital`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `patient_details`
--
ALTER TABLE `patient_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `testlist`
--
ALTER TABLE `testlist`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `todos`
--
ALTER TABLE `todos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `usersdetails`
--
ALTER TABLE `usersdetails`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
