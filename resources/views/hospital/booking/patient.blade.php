<!doctype html>
<html class="no-js " lang="en">

@include('common.head')


<body class="theme-blush">

    <!-- Page Loader -->
    @include('common.pageloader')


    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>

    <!-- Main Search -->
    <div id="search">
        <button id="close" type="button" class="close btn btn-primary btn-icon btn-icon-mini btn-round">x</button>
        <form>
            <input type="search" value="" placeholder="Search..." />
            <button type="submit" class="btn btn-primary">Search</button>
        </form>
    </div>

    <!-- Right Icon menu Sidebar -->
    @include('hospital.common.leftSideNav')
    <!-- Left Sidebar -->
    @include('hospital.common.rightSideNav')



    <section class="content">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Patient-Details</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Helium</a></li>
                            <li class="breadcrumb-item active">Patient-Details</li>
                        </ul>
                        <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>

                        <a href="{{route('booking.hospital')}}" class="btn btn-success btn-icon float-right" type="button">
                            <!--                        <i class="zmdi zmdi-plus"></i>-->
                            Booking-List</a>

                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12">
                        <div class="row clearfix">
                            <div class="col-md-12 col-sm-12">
                                <div class="card">
                                    <div class="body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5 class="mt-3"><b>Person Details</b></h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label>Patient Name</label>
                                                <p>{{$bookings->userList->name}}</p>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Age</label>
                                                <p>{{\Carbon\Carbon::parse($bookings->userList->userDetails->age)->diff(\Carbon\Carbon::now())->format('%y years, %m months and %d days')}}</p>
                                            </div>
                                            {{-- <div class="col-md-4">
                                                <label>Years</label>
                                                <p>3:30 PM</p>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Month</label>
                                                <p>3:30 PM</p>
                                            </div> --}}
                                            <div class="col-md-4">
                                                <label>Gender</label>
                                                <p>{{$bookings->userList->userDetails->gender ?? '--'}}</p>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Ph. No.</label>
                                                <p>{{$bookings->userList->phone_no ?? '--'}}</p>
                                            </div>
                                            {{-- <div class="col-md-4">
                                                <label>Nationality</label>
                                                <p>Indian</p>
                                            </div> --}}
                                            <div class="col-md-12">
                                                <label>Address</label>
                                                <p>{{$bookings->userList->userDetails->address}}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5 class="mt-3"><b>Is it a repeated sample?</b></h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label>Sample collection facility name?</label>
                                                <p>wrfwetetge</p>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Collection facility pin-code</label>
                                                <p>9993939</p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5 class="mt-3"><b>Patient Category</b></h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label>Symtomatic International Traveller in last 14 days?<span class="check-history">(Yes/No)</span></label>
                                            </div>
                                            <div class="col-md-12">
                                                <label>Symtomatic contact of lab confirmed case?<span class="check-history">(Yes/No)</span></label>
                                            </div>
                                            <div class="col-md-12">
                                                <label>Symtomatic healthcare worker.<span class="check-history">(Yes/No)</span></label>
                                            </div>
                                            <div class="col-md-12">
                                                <label>Hospitalized SARI (Severe Acute Respiratory illness) patient.<span class="check-history">(Yes/No)</span></label>
                                            </div>
                                            <div class="col-md-12">
                                                <label>Asymtomatic direct and high risk contact of confirmed case-family member.<span class="check-history">(Yes/No)</span></label>
                                            </div>
                                            <div class="col-md-12">
                                                <label>Asymtomatic healthcare worker in contact with confirmed case without adequate protection. <span class="check-history">(Yes/No)</span></label>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5 class="mt-3"><b>Person Details</b></h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Pin Code</label>
                                                <p>{{$bookings->userList->userDetails->pin ?? '--'}}</p>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Date of Birth</label>
                                                <p>{{\Carbon\Carbon::parse($bookings->userList->userDetails->age)->format('Y M d')}}</p>
                                            </div>
                                            {{-- <div class="col-md-4">
                                                <label>Patient Passport No.</label>
                                                <p>abu</p>
                                            </div> --}}
                                            <div class="col-md-4">
                                                <label>Email Id</label>
                                                <p>{{$bookings->userList->email}}</p>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Aadhar No.</label>
                                                <p>276540987612</p>
                                            </div>
                                            <div class="col-md-12">
                                                <label>Address</label>
                                                <p>{{$bookings->userList->userDetails->address}}</p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5 class="mt-3"><b>Exposure History</b></h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label>Did you travel to foreign country in last 14 days.<span class="check-history">(Yes/No)</span></label>
                                                <p>If yes,place of travel <span>asdd</span> stay/ travel duration 10/09/2020 to 15/10/2020</p>
                                            </div>
                                            <div class="col-md-12">
                                                <label>Have you been in contact with lab confirmed COVID-19 patient<span class="check-history">(Yes/No)</span></label>
                                                <p>If yes,name of confirmed patient <span>w2rdwrwrr</span> </p>
                                            </div>
                                            <div class="col-md-12">
                                                <label>Were you Quarantined?<span class="check-history">(Yes/No)</span></label>
                                                <p>If yes,where were you quarantined <span>w2rdwrwrr</span> </p>
                                            </div>
                                            <div class="col-md-12">
                                                <label>Are you a health care worker in hospital involved in managing patients. <span class="check-history">(Yes/No)</span></label>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5 class="mt-3"><b>Clinical Symtoms and Signs</b></h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="col-md-12">
                                                    <label>Cough<span class="check-history">(Yes/No)</span></label>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>Breathlessness<span class="check-history">(Yes/No)</span></label>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>Soar Throat<span class="check-history">(Yes/No)</span></label>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>Sputum<span class="check-history">(Yes/No)</span></label>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>diarrhoea<span class="check-history">(Yes/No)</span></label>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>Nausea<span class="check-history">(Yes/No)</span></label>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>Shest Pain<span class="check-history">(Yes/No)</span></label>
                                                </div>

                                            </div>

                                            <div class="col-md-6">
                                                <div class="col-md-12">
                                                    <label>Vomiting<span class="check-history">(Yes/No)</span></label>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>Haemoptysis<span class="check-history">(Yes/No)</span></label>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>Nasal Discharge<span class="check-history">(Yes/No)</span></label>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>Fever at evaluation<span class="check-history">(Yes/No)</span></label>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>Abdominal Pain<span class="check-history">(Yes/No)</span></label>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>Body Ache<span class="check-history">(Yes/No)</span></label>
                                                </div>

                                            </div>
                                        </div>



                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5 class="mt-3"><b>Underlying Medical Conditions</b></h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">

                                                <div class="col-md-12">
                                                    <label>COPD<span class="check-history">(Yes/No)</span></label>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>Chronic Renal Disease<span class="check-history">(Yes/No)</span></label>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>Bronshitis<span class="check-history">(Yes/No)</span></label>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>Malignancy<span class="check-history">(Yes/No)</span></label>
                                                </div>

                                            </div>

                                            <div class="col-md-6">
                                                <div class="col-md-12">
                                                    <label>Diabetes<span class="check-history">(Yes/No)</span></label>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>Heart Disease<span class="check-history">(Yes/No)</span></label>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>Hypertension<span class="check-history">(Yes/No)</span></label>
                                                </div>
                                                <div class="col-md-12">
                                                    <label>Asthma<span class="check-history">(Yes/No)</span></label>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5 class="mt-3"><b>Hospitalization Treatment and Investigation</b></h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Hospitalization Date</label>
                                                <p>12/10/2020</p>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Diagnosis</label>
                                                <p>qwdwqdw</p>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Differential Diagnosis</label>
                                                <p>qwdwqdw</p>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Etiology Identified</label>
                                                <p>qwdwqdw</p>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Phone Number</label>
                                                <p>qwdwqdw</p>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Hospital Name Address</label>
                                                <p>qwdwqdw</p>
                                            </div>

                                            <div class="col-md-6">
                                                <label>Nme of the doctor</label>
                                                <p>qwdwqdw</p>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Date</label>
                                                <p>{{$bookings->created_at}}</p>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5 class="mt-3"><b>Report</b></h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label>Patient Report!! </label>
                                                <p><a type="button" data-color="indigo" data-toggle="modal"
                                                    data-target="#smallModal" class="btn bg-indigo">Report</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Jquery Core Js -->
    @include('common.javascript')
</body>

</html>
