<!doctype html>
<html class="no-js " lang="en">
@include('common.head')

<body class="theme-blush">

    <!-- Page Loader -->
    @include('common.pageloader')


    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>

    <!-- Main Search -->
    <div id="search">
        <button id="close" type="button" class="close btn btn-primary btn-icon btn-icon-mini btn-round">x</button>
        <form>
            <input type="search" value="" placeholder="Search..." />
            <button type="submit" class="btn btn-primary">Search</button>
        </form>
    </div>

        <!-- Right Icon menu Sidebar -->
        @include('hospital.common.leftSideNav')
        <!-- Left Sidebar -->
        @include('hospital.common.rightSideNav')


    <section class="content">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Booking History</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i>Helium</a></li>
                            <li class="breadcrumb-item active">Booking History</li>
                        </ul>
                        <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
                      <a href="{{route('booking.hospital')}}" class="btn btn-success btn-icon float-right" type="button">
<!--                        <i class="zmdi zmdi-plus"></i>-->
                     Booking List</a>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12">
                        @foreach ($bookings as $item)
                        <div class="card">
                            <div class="body">
                                <div class="row">
                                    <div class="col-lg-3 col-md-12">
                                        <small class="text-muted">Name: </small>
                                        <p>{{$item->userList->name}}</p>
                                    </div>
                                    <div class="col-lg-3 col-md-12">
                                        <small class="text-muted">Date of Booking: </small>
                                        <p>{{\Carbon\Carbon::parse($item->booking_date)->format('Y M d')}}</p>
                                    </div>
                                    <div class="col-lg-3 col-md-12">
                                        <small class="text-muted">Time: </small>
                                        <p>{{\Carbon\Carbon::parse($item->booking_date)->format('h:i:s')}}</p>
                                    </div>
                                    <div class="col-lg-3 col-md-12">
                                        <small class="text-muted">Phone Number: </small>
                                        <p>{{$item->userList->phone_no}}</p>
                                    </div>
                                    <div class="col-lg-3 col-md-12">
                                        <small class="text-muted">Email: </small>
                                        <p>{{$item->userList->email}}</p>
                                    </div>
                                    <div class="col-lg-3 col-md-12">
                                        <small class="text-muted">Zip Code: </small>
                                        <p>{{$item->hospitalList->hospitalDetails->pin}}</p>
                                    </div>
                                     <div class="col-lg-3 col-md-12">
                                        <small class="text-muted">Type of Test: </small>
                                        <p>Rapid Antigen test</p>
                                    </div>
                                     <div class="col-lg-3 col-md-12">
                                        <small class="text-muted">Test Location: </small>
                                        <p>Hospital visit</p>
                                    </div>
                                    <div class="col-lg-3 col-md-12">
                                        <small class="text-muted">Hospital Name: </small>
                                        <p>{{$item->hospitalList->hospitalDetails->hospital_name}}</p>
                                    </div>
                                    <div class="col-lg-3 col-md-12">
                                        <small class="text-muted">Status: </small>
                                        <p>Completed</p>
                                    </div>
                                    <div class="col-lg-3 col-md-12">
                                        <small class="text-muted">Assigned Collection Agent: </small>
                                        <p>{{$item->agentList->name ?? '--'}}</p>
                                    </div>
                                    <div class="col-lg-3 col-md-12">
                                        <small class="text-muted">Report: </small>
                                        <p>Negative</p>
                                    </div>
                                    <div class="col-lg-12 col-md-12">
                                        <small class="text-muted">Address: </small>
                                        <p>{{$item->hospitalList->hospitalDetails->address ?? '--'}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- Jquery Core Js -->
   @include('common.javascript')
</body>

</html>
