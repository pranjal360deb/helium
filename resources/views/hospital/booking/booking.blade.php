<!doctype html>
<html class="no-js " lang="en">

@include('common.head')

<body class="theme-blush">

    <!-- Page Loader -->

    @include('common.pageloader')

    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>

    <!-- Main Search -->
    <div id="search">
        <button id="close" type="button" class="close btn btn-primary btn-icon btn-icon-mini btn-round">x</button>
        <form>
            <input type="search" value="" placeholder="Search..." />
            <button type="submit" class="btn btn-primary">Search</button>
        </form>
    </div>

    <!-- Right Icon menu Sidebar -->
    @include('hospital.common.leftSideNav')

    <!-- Left Sidebar -->
    @include('hospital.common.rightSideNav')




    <section class="content">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Bookings</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Helium</a>
                            </li>
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Bookings</a></li>
                        </ul>
                        <button class="btn btn-primary btn-icon mobile_menu" type="button"><i
                                class="zmdi zmdi-sort-amount-desc"></i></button>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i
                                class="zmdi zmdi-arrow-right"></i></button>
                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <!-- Basic Examples -->
                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="body">
                                <div class="row">
                                    <div class="col-md-8">
                                        <p>List of Bookings
                                        </p>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="list-inline hospital-filter">
                                            <li class="hospital-filter1">
                                                <!-- Search form -->
                                                <input class="form-control search" type="text" placeholder="Search"
                                                    aria-label="Search">
                                            </li>
                                            <li class="hospital-filter1">
                                                <div class="box1">
                                                    <select name="isum2" class="wide selectpicker form-control">
                                                        <option value="hidden">Status</option>
                                                        <option value="xxx">Scheduled</option>
                                                        <option value="xxx">Sample collected</option>
                                                        <option value="xxx">Sample dispatched</option>
                                                        <option value="xxx">Completed </option>
                                                    </select>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Sl No.</th>
                                                <th>Booking Id</th>
                                                <th>Name</th>
                                                <th>Date</th>
                                                <th>Zip Code</th>
                                                <th>Type of Test</th>
                                                {{-- <th style="width:15%;">Test Location
                                                </th> --}}
                                                <th>OTP</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($bookings as $index => $item)
                                                <tr>
                                                    <td>{{ $index + 1 }}</td>
                                                    <td>{{ $item->booking_id }}</td>
                                                    <td>{{ $item->userList->name }}</td>
                                                    <td>{{ $item->booking_date }}</td>
                                                    <td>{{ $item->userList->userDetails->pin ?? '--' }}</td>
                                                    <td>RTPCR</td>
                                                    <td>
                                                        @if ($item->is_deposited == 1)
                                                            Verified
                                                        @else
                                                            <button type="button" onclick="verifyOTP('{{ $item->id }}')"
                                                                data-color="indigo" data-toggle="modal"
                                                                data-target="#smallModal"
                                                                class="btn bg-indigo">Verify</button>
                                                        @endif

                                                    </td>
                                                    <td>
                                                        <a href="{{ route('hosPanel.booking.details', ['id' => $item->user_id]) }}"
                                                            class="btn btn-info">View</a>
                                                        <a href="{{ route('hosPanel.patient.profile', ['id' => $item->id]) }}"
                                                            class="btn btn-info">Details
                                                        </a>
                                                    </td>

                                                </tr>

                                            @endforeach
                                        </tbody>
                                    </table>
                                    <div class="d-flex justify-content-end">
                                        {{ $bookings->links() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Exportable Table -->

            </div>

        </div>
    </section>


    <div class="modal fade" id="smallModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <form class="row clearfix" method="post" id="verifyOTP">
                        @csrf
                        <input type="text" name="id" id="id" style="display: none">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Generate OTP</label>
                                <input type="text" class="form-control" name="otp" placeholder="OTP" />
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-raised btn-primary btn-round">Submit</button>
                        </div>
                    </form>


                </div>

            </div>
        </div>
    </div>


    <!-- Jquery Core Js -->
    @include('common.javascript')
    <!-- Select2 Js -->
    <script>
        function verifyOTP(id) {
            document.getElementById('id').value = id;
            document.getElementById('verifyOTP').action = '{{route('otp.verification')}}';
        }

    </script>

</body>

</html>
