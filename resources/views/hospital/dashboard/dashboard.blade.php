<!doctype html>
<html class="no-js " lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
    <title>Helium</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon"> <!-- Favicon-->
    <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/plugins/jvectormap/jquery-jvectormap-2.0.3.min.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/plugins/charts-c3/plugin.css')}}" />

    <link rel="stylesheet" href="{{asset('assets/plugins/morrisjs/morris.min.css')}}" />

    <!-- Bootstrap Select Css -->
    <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-select/css/bootstrap-select.css')}}" />
    <!-- Select2 -->
    <link rel="stylesheet" href="{{asset('assets/plugins/select2/select2.css')}}" />


    <!-- Custom Css -->
    <link rel="stylesheet" href="{{asset('assets/css/style.min.css')}}">





</head>

<body class="theme-blush">

    <!-- Page Loader -->
   @include('common.pageloader')

    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>

    <!-- Main Search -->
    <div id="search">
        <button id="close" type="button" class="close btn btn-primary btn-icon btn-icon-mini btn-round">x</button>
        <form>
            <input type="search" value="" placeholder="Search..." />
            <button type="submit" class="btn btn-primary">Search</button>
        </form>
    </div>

    <!-- Right Icon menu Sidebar -->
    @include('hospital.common.leftSideNav')

    <!-- Left Sidebar -->
    @include('hospital.common.rightSideNav')




    <!-- Main Content -->

    <section class="content">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>Dashboard</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i>Helium</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ul>
                    <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="card widget_2 big_icon traffic">
                        <div class="body">
                            <h6>Total Booking</h6>
                            <h2>{{$bookingCount}}</h2>
                            <small>Test received till date from users.</small>
                            <div class="progress">
                                <div class="progress-bar l-amber" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="card widget_2 big_icon email">
                        <div class="body">
                            <h6>Test Completed</h6>
                            <h2>39</h2>
                            <small>Test completed till date from users.</small>
                            <div class="progress">
                                <div class="progress-bar l-purple" role="progressbar" aria-valuenow="39" aria-valuemin="0" aria-valuemax="100" style="width: 39%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="card widget_2 big_icon domains">
                        <div class="body">
                            <h6>Test Scheduled</h6>
                            <h2>8</h2>
                            <small>Test completed till date from users.</small>
                            <div class="progress">
                                <div class="progress-bar l-green" role="progressbar" aria-valuenow="89" aria-valuemin="0" aria-valuemax="100" style="width: 89%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="body">
                            <div class="row">
                                <div class="col-md-6">
                                    <p>List of Bookings
                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <div class="float-right">

                                        <form action="{{ route('hosPanel.search') }}" method="post">
                                            {{-- <div class="mb-3">
                                                --}}

                                                @csrf
                                                <ul class="list-inline hospital-filter">
                                                    <li class="hospital-filter1">
                                                        <select name="day" class="form-control show-tick">
                                                            <option value="3">All</option>
                                                            <option value="1">Today</option>
                                                            <option value="2">Tomorrow</option>
                                                        </select>
                                                    </li>

                                                    <li class="hospital-filter1">

                                                        <button type="submit"
                                                            class="btn btn-raised btn-primary btn-round">Search</button>

                                                    </li>
                                                </ul>
                                                {{--
                                            </div> --}}
                                        </form>
                                    </div>

                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Sl No.</th>
                                            <th>Name</th>
                                            <th>Date</th>
                                            <th>Time</th>
                                            <th>Ph No.</th>
                                            {{-- <th>Member</th> --}}
                                            <th>Test Type</th>
                                            {{-- <th>Status</th> --}}
                                            <th>Zip Code</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($authBook as $index=>$item)
                                       <tr>
                                           <td>{{$index+1}}</td>
                                           <td>{{ $item->userList->name }}</td>
                                           <td>{{ \Carbon\Carbon::parse($item->booking_date)->format('Y M d') }}</td>
                                           <td>{{ \Carbon\Carbon::parse($item->booking_date)->format('h:i:s') }}</td>
                                           <td>{{ $item->userList->phone_no ?? '--' }}</td>
                                           <td>RT-PCR</td>
                                           <td>{{ $item->userList->userDetails->pin ?? '--' }}</td>
                                           <td><a href="{{ route('hosPanel.booking.details', ['id' => $item->user_id]) }}"
                                            class="btn btn-info">View</a></td>
                                       </tr>

                                       @endforeach

                                    </tbody>
                                </table>
                                <div class="d-flex justify-content-end">
                                    {{ $authBook->links() }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12"><a href="{{route('booking.hospital')}}" class="btn btn-info">View All</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="body">

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs p-0 mb-3">
                                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#home">Weekly Booking Record</a></li>
                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#profile">Monthly Booking Record</a></li>
                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#messages">Yearly Booking Record</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane in active" id="home">
                                    <b>Weekly Booking Record</b>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <canvas id="myChart"></canvas>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="profile">
                                    <b>Monthly Booking Record</b>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <canvas id="myChart1"></canvas>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="messages">
                                    <b>Yearly Booking Record</b>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <canvas id="myChart2"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </section>


    <!-- Jquery Core Js -->
    {{-- <script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js ( jquery.v3.2.1, Bootstrap4 js) -->
    <script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->

    <!--    <script src="assets/bundles/jvectormap.bundle.js"></script>  JVectorMap Plugin Js -->
    <script src="assets/bundles/sparkline.bundle.js"></script> <!-- Sparkline Plugin Js -->
    <script src="assets/bundles/c3.bundle.js"></script>

    <script src="assets/bundles/mainscripts.bundle.js"></script>
    <script src="assets/js/pages/index.js"></script>

    <script src="assets/plugins/select2/select2.min.js"></script>
    <!-- Select2 Js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.2/Chart.min.js"></script> --}}

    @include('common.javascript')
    <script>

        var ctx = document.getElementById("myChart").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
                datasets: [{
                    label: 'bookings',
                    data: [ {!! json_encode($bookingMondayCount) !!}, {!!json_encode($bookingTuesdayCount)!!}, {!!json_encode($bookingWednesdayCount)!!}, {!!json_encode($bookingThrusdayCount)!!}, {!!json_encode($bookingFridayCount)!!}, {!! json_encode($bookingSaturdayCount) !!}, {!! json_encode($bookingSundayCount) !!}],
                    backgroundColor: "rgb(228 114 151 / 89%)"
                }]
            }
        });
    </script>

    <script>
        var ctx = document.getElementById("myChart1").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                datasets: [{
                    label: 'bookings',
                    data: [{!!json_encode($januaryArrayCount)!!}, {!!json_encode($febArrayCount)!!}, {!!json_encode($marArrayCount)!!}, {!!json_encode($aprArrayCount)!!}, {!!json_encode($mayArrayCount)!!}, {!!json_encode($juneArrayCount)!!}, {!!json_encode($julyArrayCount)!!}, {!!json_encode($augustArrayCount)!!}, {!!json_encode($septArrayCount)!!}, {!!json_encode($octoberArrayCount)!!}, {!!json_encode($novArrayCount)!!}, {!!json_encode($decArrayCount)!!}],
                    backgroundColor: "rgb(228 114 151 / 89%)"
                }]
            }
        });
    </script>

     <script>
        var ctx = document.getElementById("myChart2").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["2020", "2021", "2022", "2023", "2024", "2025"],
                datasets: [{
                    label: 'bookings',
                    data: [{!!json_encode($firstYearArrayCount)!!}, {!!json_encode($secYearArrayCount)!!}, {!!json_encode($thirdYearArrayCount)!!}, {!!json_encode($fourthYearArrayCount)!!}, {!!json_encode($fifthYearArrayCount)!!}, {!!json_encode($sixthYearArrayCount)!!}],
                    backgroundColor: "rgb(228 114 151 / 89%)"
                }]
            }
        });
    </script>



</body>

</html>
