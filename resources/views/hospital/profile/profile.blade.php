<!doctype html>
<html class="no-js " lang="en">

@include('common.head')

<body class="theme-blush">

    <!-- Page Loader -->
    @include('common.pageloader')

    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>

    <!-- Main Search -->
    <div id="search">
        <button id="close" type="button" class="close btn btn-primary btn-icon btn-icon-mini btn-round">x</button>
        <form>
            <input type="search" value="" placeholder="Search..." />
            <button type="submit" class="btn btn-primary">Search</button>
        </form>
    </div>

    <!-- Right Icon menu Sidebar -->

    @include('hospital.common.leftSideNav')

    <!-- Left Sidebar -->
    @include('hospital.common.rightSideNav')



    <section class="content">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Hospital Profile</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Helium</a></li>
                            <li class="breadcrumb-item active">Hospital Profile</li>
                        </ul>
                        <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>

                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row clearfix">

                    <div class="col-lg-12 col-md-12">
                        <div class="row clearfix">
                            <div class="col-md-12 col-sm-12">
                                <div class="card">
                                    <div class="body">
                                        <!--
                                        <a href="https://themeforest.net/item/arrowlite-responsive-admin-dashboard-template/23656497">
                                            <img class="img-fluid img-thumbnail" src="http://www.wrraptheme.com//templates/preview/arrowlite.png" alt="">
                                        </a>
-->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5 class="mt-3"><b>{{$profile->name}}</b></h5>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Phone No.</label>
                                                <p>{{$profile->phone_no ?? '--'}}</p>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Email Id</label>
                                                <p>{{$profile->email ?? '--'}}</p>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Registration No.</label>
                                                <p>{{$profile->hospitalDetails->registration_number ?? '--'}}</p>
                                            </div>
                                             {{-- <div class="col-md-6">
                                                <label>Status</label>
                                                <p>xxx</p>
                                            </div> --}}
                                            <div class="col-md-6">
                                                <label>Date</label>
                                                <p>{{$profile->created_at}}</p>
                                            </div>
                                             <div class="col-md-6">
                                                <label>Zip Code</label>
                                                <p>{{$profile->hospitalDetails->pin}}</p>
                                            </div>


                                            <div class="col-md-12">
                                                <label>Address</label>
                                                <p>{{$profile->hospitalDetails->address}}</p>
                                            </div>
                                        </div>
                                        {{-- <div class="justify-content-between">
                                            <a href="booking.html" class="btn btn-info">View Bookings</a>
                                            <a href="#" class="btn btn-danger">Remove</a>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Jquery Core Js -->
   @include('common.javascript')
</body>

</html>
