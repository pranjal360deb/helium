<aside id="leftsidebar" class="sidebar">
    <div class="navbar-brand">
        <button class="btn-menu ls-toggle-btn" type="button"><i class="zmdi zmdi-menu"></i></button>
        <a href="{{route('hosPanel.dashboard')}}"><img src="{{asset('assets/images/logo.svg')}}" width="25" alt="Aero"><span class="m-l-10">Aero</span></a>
    </div>
    <div class="menu">
       <ul class="list">
            <li>
                <div class="user-info">
                    {{-- <a class="image" href="{{route('hosPanel.profile', ['id'=>Auth::user()->id])}}"><img src="{{asset('assets/images/profile_av.jpg')}}" alt="User"></a> --}}
                    <a class="image" href="{{route('hosPanel.profile')}}"><img src="{{asset('assets/images/profile_av.jpg')}}" alt="User"></a>
                    <div class="detail">
                        <h4>{{Auth::user()->name}}</h4>
                        <small>Hospital</small>
                    </div>
                </div>
            </li>
            <li {!! (Request::is('hospitalPanel/dashboard') || Request::is('hospitalPanel/dashboard/*') ? ' class="active open"' : '') !!}><a href="{{route('hosPanel.dashboard')}}"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a></li>
            <li {!! (Request::is('hospitalPanel/booking') || Request::is('hospitalPanel/booking/*') ? ' class="active open"' : '') !!}><a href="{{route('booking.hospital')}}"><i class="zmdi zmdi-check-circle"></i><span>Bookings</span></a></li>
            {{-- <li><a href="booking.html"><i class="zmdi zmdi-airline-seat-individual-suite"></i><span>Patinets</span></a></li> --}}
            <li {!! (Request::is('hospitalPanel/settings') || Request::is('hospitalPanel/settings/*') ? ' class="active open"' : '') !!}><a href="{{route('settings.hospital')}}"><i class="zmdi zmdi-settings"></i><span>Settings</span></a></li>

        </ul>
    </div>
</aside>
