<!doctype html>
<html class="no-js " lang="en">

@include('common.head')

<body class="theme-blush">

    <!-- Page Loader -->
    @include('common.pageloader')


    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>

    <!-- Main Search -->
    <div id="search">
        <button id="close" type="button" class="close btn btn-primary btn-icon btn-icon-mini btn-round">x</button>
        <form>
            <input type="search" value="" placeholder="Search..." />
            <button type="submit" class="btn btn-primary">Search</button>
        </form>
    </div>

        <!-- Right Icon menu Sidebar -->
    @include('hospital.common.leftSideNav')

    <!-- Left Sidebar -->
    @include('hospital.common.rightSideNav')


    <section class="content contact">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Notifications</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i>Heluim</a></li>
                            <li class="breadcrumb-item active">Notifications</li>
                        </ul>
                        <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="table-responsive">
                                <table class="table table-hover mb-0 c_list c_table">
                                    <thead>
                                        <tr>
                                            <th style="width: 10%;">Sl No.</th>
                                            <th>Notification</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {{-- <tr>
                                            <td>1 </td>
                                            <td>
                                                <p class="c_name">John Smith</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>2 </td>
                                            <td>
                                                <p class="c_name">John Smith</p>
                                            </td>
                                        </tr> --}}
                                        @foreach (auth()->user()->notifications as $index => $item)
                                            <tr>
                                                <td>{{$index+1}}</td>
                                                <td>{{$item->data['name']}}, {{$item->data['details']}}</td>
                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Jquery Core Js -->
  @include('common.javascript')
</body>

</html>
