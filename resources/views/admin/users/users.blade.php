<!doctype html>
<html class="no-js " lang="en">

@include('common.head')

<body class="theme-blush">

    <!-- Page Loader -->
    @include('common.pageloader')


    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>

    <!-- Main Search -->
    <div id="search">
        <button id="close" type="button" class="close btn btn-primary btn-icon btn-icon-mini btn-round">x</button>
        <form>
            <input type="search" value="" placeholder="Search..." />
            <button type="submit" class="btn btn-primary">Search</button>
        </form>
    </div>

    <!-- Right Icon menu Sidebar -->
    @include('admin.common.rightsidebar')
    <!-- Left Sidebar -->
    @include('admin.common.leftsidebar')




    <section class="content">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Users</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Helium</a>
                            </li>
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Users</a></li>
                        </ul>
                        <button class="btn btn-primary btn-icon mobile_menu" type="button"><i
                                class="zmdi zmdi-sort-amount-desc"></i></button>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i
                                class="zmdi zmdi-arrow-right"></i></button>

                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <!-- Basic Examples -->
                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="body">
                                <div class="row">
                                    <div class="col-md-2">
                                        <p>List of Agents
                                        </p>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="float-right">

                                            <form action="{{ route('user.search') }}" method="post">
                                                @csrf
                                                <ul class="list-inline hospital-filter">
                                                    <li class="hospital-filter1">
                                                        <!-- Search form -->
                                                        <input class="form-control search" name="name" type="text"
                                                            placeholder="Search" aria-label="Search">
                                                    </li>
                                                    <li class="hospital-filter1">
                                                        <div class="box1">
                                                            <select name="status" class="wide selectpicker form-control">
                                                                <option value="" selected disabled>Select Status</option>
                                                                <option value='1'>Active</option>
                                                                <option value='0'>Inactive</option>
                                                            </select>
                                                        </div>
                                                    </li>
                                                    <li class="hospital-filter1">
                                                        <button type="submit"
                                                            class="btn btn-raised btn-primary btn-round">Search</button>
                                                    </li>
                                                </ul>
                                            </form>
                                        </div>

                                    </div>

                                </div>

                                <div class="table-responsive">
                                    <table class="table  table-striped">
                                        <thead>
                                            <tr>
                                                <th>Sl No.</th>
                                                <th>Name</th>
                                                <th>Ph No.</th>
                                                <th>Email</th>
                                                <th>Zip Code</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($users as $index => $item)
                                                <tr>
                                                    <td>{{ $index + 1 }}</td>
                                                    <td>{{ $item->name }}</td>
                                                    <td>{{ $item->phone_no ?? '--' }}</td>
                                                    <td>{{ $item->email }}</td>
                                                    <td>{{ $item->userDetails->pin ?? '--' }}</td>
                                                    <td style="width: 18%">
                                                        @if ($item->is_deactivate == 1)
                                                            <label class="switch">
                                                                <input id="testingUpdate" data-id="{{ $item->id }}" class="toggle-deactive"
                                                                    type="checkbox" checked>
                                                                <span class="slider round">

                                                                </span>
                                                            </label>
                                                        @else
                                                            <label class="switch">
                                                                <input id="testingUpdate" data-id="{{ $item->id }}" class="toggle-active"
                                                                    type="checkbox">
                                                                <span class="slider round">

                                                                </span>
                                                            </label>

                                                        @endif
                                                    </td>
                                                    <td>
                                                        <ul class="list-inline hospital-action">
                                                            <li><a href="{{ route('users.view', ['id' => $item->id]) }}"
                                                                    class="btn btn-info">View</a></li>
                                                            <li><a href="hospital-profile.html" data-toggle="modal"
                                                                    data-target="#smallModal"
                                                                    class="btn btn-danger">Remove</a></li>
                                                        </ul>
                                                    </td>
                                                </tr>

                                            @endforeach





                                        </tbody>
                                    </table>
                                    <div class="d-flex justify-content-end">
                                        {{ $users->links() }}
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <!-- Exportable Table -->

            </div>

        </div>
    </section>

    <div class="modal fade" id="smallModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <h5 class="modal-heading">Are You Sure ?</h5>
                    <p>Do you really want to delete these records? This process cannot be undone.</p>
                    <div>
                        <ul class="list-inline modal-des">
                            <li><button type="button" class="btn btn-info">Yes</button></li>
                            <li><button type="button" class="btn btn-danger" data-dismiss="modal">No</button></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <!-- Jquery Core Js -->
    @include('common.javascript')
    <script>

        $(document.body).on('change', '#testingUpdate', function() {
            var status = $(this).prop('checked') == true ? 1 : 0;
            var user_id = $(this).data('id');
            console.log(status);
            var formDat = {
                id: user_id,
                value: status
            }
            // console.log(formDat);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "post",
                // dataType: "json",
                // _token: "{{ csrf_token() }}",
                url: '/activeAgent',
                // data: {'status': status, 'user_id': user_id},
                data: formDat,
                success: function(data) {
                    console.log(data)
                }
            });
            // alert(user_id);
        });
        // $(function() {
        //     $('.toggle-active').change(function() {
        //         var status = $(this).prop('checked') == true ? 1 : 0;
        //         var user_id = $(this).data('id');
        //         console.log(status);
        //         var formDat = {
        //             id: user_id,
        //             value: status
        //         }
        //         // console.log(formDat);

        //         $.ajax({
        //             headers: {
        //                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //             },
        //             type: "post",
        //             // dataType: "json",
        //             // _token: "{{ csrf_token() }}",
        //             url: 'activeAgent',
        //             // data: {'status': status, 'user_id': user_id},
        //             data: formDat,
        //             success: function(data) {
        //                 console.log(data)
        //             }
        //         });
        //     })

        //     $('.toggle-deactive').change(function() {
        //         var status = $(this).prop('checked') == true ? 1 : 0;
        //         var user_id = $(this).data('id');
        //         console.log(status);
        //         var formDat = {
        //             id: user_id,
        //             value: status
        //         }
        //         // console.log(formDat);

        //         $.ajax({
        //             headers: {
        //                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //             },
        //             type: "post",
        //             // dataType: "json",
        //             // _token: "{{ csrf_token() }}",
        //             url: 'activeAgent',
        //             // data: {'status': status, 'user_id': user_id},
        //             data: formDat,
        //             success: function(data) {
        //                 console.log(data)
        //             }
        //         });
        //     })
        // })

    </script>

</body>

</html>
