<!doctype html>
<html class="no-js " lang="en">

@include('common.head')

<body class="theme-blush">

    <!-- Page Loader -->
    @include('common.pageloader')


    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>

    <!-- Main Search -->
    <div id="search">
        <button id="close" type="button" class="close btn btn-primary btn-icon btn-icon-mini btn-round">x</button>
        <form>
            <input type="search" value="" placeholder="Search..." />
            <button type="submit" class="btn btn-primary">Search</button>
        </form>
    </div>

   <!-- Right Icon menu Sidebar -->
   @include('admin.common.rightsidebar')
   <!-- Left Sidebar -->
   @include('admin.common.leftsidebar')



    <section class="content">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>User Profile</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Helium</a></li>
                            <li class="breadcrumb-item active">User Profile</li>
                        </ul>
                        <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>

                        <a href="{{route('users')}}" class="btn btn-success btn-icon float-right" type="button">
                            <!--                        <i class="zmdi zmdi-plus"></i>-->
                            Users List</a>

                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row clearfix">

                    <div class="col-lg-12 col-md-12">
                        <div class="row clearfix">
                            <div class="col-md-12 col-sm-12">
                                <div class="card">
                                    <div class="body">
                                        <!--
                                        <a href="https://themeforest.net/item/arrowlite-responsive-admin-dashboard-template/23656497">
                                            <img class="img-fluid img-thumbnail" src="http://www.wrraptheme.com//templates/preview/arrowlite.png" alt="">
                                        </a>
-->
                                        <div class="row">
                                            <div class="col-md-9">
                                                <h5 class="mt-3"><b>{{$hosName}}</b></h5>
                                            </div>
                                            {{-- <div class="col-md-3">
                                                <div class="active-btn">
                                                    <input class="checkbox chckBtn" id="checkbox1" type="checkbox" />
                                                    <label for="checkbox1" class="checkbox-label">
                                                        <span class="on ">Active</span>
                                                        <span class="off">Deactive</span>
                                                    </label>
                                                </div>
                                            </div> --}}
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Date of Registration</label>
                                                <p>{{$hosDateRegis}}</p>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Time</label>
                                                <p>{{$hosDateRegis}}</p>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Ph No.</label>
                                                <p>{{$hosPhoneNo ?? '--'}}</p>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Email</label>
                                                <p>{{$hosEmail}}</p>
                                            </div>
                                            {{-- <div class="col-md-6">
                                                <label>Status</label>
                                                <p>Scheduled</p>
                                            </div> --}}
                                            <div class="col-md-6">
                                                <label>Zip Code</label>
                                                <p>788898</p>
                                            </div>
                                            {{-- <div class="col-md-6">
                                                <label>Type of test</label>
                                                <p>Rapid Antigen test</p>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Test Location</label>
                                                <p>Hospital visit</p>
                                            </div> --}}
                                            {{-- <div class="col-md-12">
                                                <label>Name Of the Hospital</label>
                                                <p>Nemcare</p>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Booked For</label>
                                                <div class="table-responsive">
                                                    <table class="table" style="font-size: 14px">
                                                        <thead>
                                                            <tr>
                                                                <th>Sl No.</th>
                                                                <th>User name</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <th scope="row">1</th>
                                                                <td>Abddd</td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row">2</th>
                                                                <td>Abddd</td>
                                                            </tr>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div> --}}
                                            <div class="col-md-12">
                                                <label>Address</label>
                                                <p>{{$hosAddress ?? '--' }} </p>
                                            </div>
                                        </div>
                                        {{-- <div class="justify-content-between">
                                             <a href="booking-history.html" class="btn btn-info">Bookings History</a>
                                            <a href="#" class="btn btn-danger">Remove</a>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Jquery Core Js -->
    @include('common.javascript')
</body>

</html>
