<!doctype html>
<html class="no-js " lang="en">

@include('common.head')

<body class="theme-blush">

    <!-- Page Loader -->
    @include('common.pageloader')


    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>

    <!-- Main Search -->
    <div id="search">
        <button id="close" type="button" class="close btn btn-primary btn-icon btn-icon-mini btn-round">x</button>
        <form>
            <input type="search" value="" placeholder="Search..." />
            <button type="submit" class="btn btn-primary">Search</button>
        </form>
    </div>

    <!-- Right Icon menu Sidebar -->
    @include('admin.common.rightsidebar')
    <!-- Left Sidebar -->
    @include('admin.common.leftsidebar')



    <section class="content">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Patient</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Helium</a></li>
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Patient</a></li>
                        </ul>
                        <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>

                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <!-- Basic Examples -->
                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="body">
                                <div class="row">
                                    <div class="col-md-8">
                                        <p>List of Patient
                                        </p>
                                    </div>
                                    <div class="col-md-4">

                                        <ul class="list-inline hospital-filter">
                                            <li class="hospital-filter1">
                                                <!-- Search form -->
<input class="form-control search" type="text" placeholder="Search" aria-label="Search">
                                            </li>
                                            <li class="hospital-filter1">
                                                <div class="box1">
                                                    <select name="isum2" class="wide selectpicker form-control">
                                                        <option value="hidden">Status</option>
                                                        <option value="xxx">Active</option>
                                                        <option value="yyy">Deactive</option>
                                                    </select>
                                                </div>
                                            </li>
                                        </ul>

                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table class="table  table-striped">
                                        <thead>
                                            <tr>
                                                <th>Sl No.</th>
                                                <th>Name</th>
                                                <th>Ph No.</th>
                                                <th>Email</th>
                                                <th>Zip Code</th>
                                                <th>Status</th>
                                                {{-- <th>created_at</th> --}}
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($bookings as $index => $item)
                                            <tr>
                                                <td>{{$index+1}}</td>
                                                <td>{{ $item->userList->name }}</td>
                                                <td>{{ $item->userList->phone_no ?? '--' }}</td>
                                                <td>{{ $item->userList->email ?? '--' }}</td>
                                                <td>{{ $item->userList->userDetails->pin ?? '--' }}</td>
                                                <td>Booked</td>
                                                {{-- <td>{{\Carbon\Carbon::parse($item->created_at)->diff(\Carbon\Carbon::now())->format('%y years, %m months and %d days')}}</td> --}}
                                                <td>
                                                    <ul class="list-inline hospital-action">
                                                        <li><a href="{{route('patient.profile',['id'=>$item->id])}}" class="btn btn-info">View</a></li>
    <!--                                                        <li><a href="hospital-profile.html" class="btn btn-danger">Remove</a></li>                                                        -->
                                                    </ul>
                                                </td>

                                            </tr>

                                            @endforeach

                                            {{-- <td style="width: 18%">
                                                <div class="">
                                                    <input class="checkbox chckBtn" id="checkbox1" type="checkbox" />
                                                    <label for="checkbox1" class="checkbox-label">
                                                        <span class="on ">Active</span>
                                                        <span class="off">Deactive</span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                <ul class="list-inline hospital-action">
                                                    <li><a href="patient-details.html" class="btn btn-info">View</a></li>
<!--                                                        <li><a href="hospital-profile.html" class="btn btn-danger">Remove</a></li>                                                        -->
                                                </ul>
                                            </td> --}}


                                        </tbody>
                                    </table>
                                    <div class="d-flex justify-content-end">
                                        {{ $bookings->links() }}
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <!-- Exportable Table -->

            </div>

        </div>
    </section>


    <!-- Jquery Core Js -->
   @include('common.javascript')

</body>

</html>
