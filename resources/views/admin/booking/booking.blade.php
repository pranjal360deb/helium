<!doctype html>
<html class="no-js " lang="en">

@include('common.head')

<body class="theme-blush">

    @include('common.pageloader')


    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>

    <!-- Main Search -->
    <div id="search">
        <button id="close" type="button" class="close btn btn-primary btn-icon btn-icon-mini btn-round">x</button>
        <form>
            <input type="search" value="" placeholder="Search..." />
            <button type="submit" class="btn btn-primary">Search</button>
        </form>
    </div>

    <!-- Right Icon menu Sidebar -->
    @include('admin.common.rightsidebar')
    <!-- Left Sidebar -->
    @include('admin.common.leftsidebar')



    <section class="content">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Bookings</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Helium</a>
                            </li>
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Bookings</a></li>
                        </ul>
                        <button class="btn btn-primary btn-icon mobile_menu" type="button"><i
                                class="zmdi zmdi-sort-amount-desc"></i></button>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i
                                class="zmdi zmdi-arrow-right"></i></button>
                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <!-- Basic Examples -->
                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <p>List of Bookings
                                        </p>
                                    </div>
                                    <div class="col-md-6">
                                        <ul class="list-inline hospital-filter">
                                            <li class="hospital-filter1">
                                                <!-- Search form -->
                                                <input class="form-control search" type="text" placeholder="Search"
                                                    aria-label="Search">
                                            </li>
                                            <li class="hospital-filter1">
                                                <div class="box1">
                                                    <select name="isum2" class="wide selectpicker form-control">
                                                        <option value="hidden">Status</option>
                                                        <option value="xxx">Scheduled</option>
                                                        <option value="xxx">Sample collected</option>
                                                        <option value="xxx">Sample dispatched</option>
                                                        <option value="xxx">Completed </option>
                                                    </select>
                                                </div>
                                            </li>
                                            <li class="hospital-filter1">

                                                <button type="button"
                                                    class="btn btn-raised btn-primary btn-round">Search</button>

                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Sl No.</th>
                                                <th>Booking Id</th>
                                                <th>Name</th>
                                                <th>Date</th>
                                                <th>Ph. No.</th>
                                                <th>Zip Code</th>
                                                <th>Type of Test</th>
                                                <th>Assign Agent</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($bookings as $index => $item)
                                                <tr>
                                                    <td>{{ $index + 1 }}</td>
                                                    <td>{{$item->booking_id}}</td>
                                                    <td>{{ $item->userList->name }}</td>
                                                    <td>{{ $item->booking_date }}</td>
                                                    <td>{{ $item->userList->phone_no ?? '--' }}</td>
                                                    <td>{{ $item->userList->userDetails->pin ?? '--' }}</td>
                                                    <td>RT-PCR</td>
                                                    <td>
                                                        @if ( $item->agent_id != null)
                                                            {{$item->agentList->name}}
                                                        @else
                                                        <button type="button" onclick="assignAgent('{{$item->id}}')"  data-color="indigo" data-toggle="modal"
                                                            data-target="#smallModal" class="btn bg-indigo">Click
                                                            here</button>

                                                        @endif


                                                    </td>
                                                    <td><a href="{{ route('booking.details',['id'=>$item->user_id]) }}"
                                                            class="btn btn-info">View</a></td>
                                                </tr>
                                            @endforeach
                                            {{-- <tr>
                                                <td>1</td>
                                                <td>001</td>
                                                <td>Tiger Nixon</td>
                                                <td>5/10/2020 3:20 PM</td>
                                                <td>91029345645</td>
                                                <td>781127</td>
                                                <td>RT-PCR</td>
                                                <td><a type="button" data-color="indigo" data-toggle="modal"
                                                        data-target="#smallModal" class="btn bg-indigo">Click here</a>
                                                </td>
                                                <td><a href="{{ route('booking.details') }}"
                                                        class="btn btn-info">View</a></td>
                                            </tr> --}}
                                        </tbody>
                                    </table>
                                    <div class="d-flex justify-content-end">
                                        {{ $bookings->links() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Exportable Table -->

            </div>

        </div>

    </section>




    <div class="modal fade" id="smallModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <form class="row clearfix" method="post" id="assignAgentForm">
                        @csrf
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Add Collection Agent</label>
                                <div class="box1">
                                    {{-- <form  >
                                        @csrf --}}
                                    <select name="agent_id" class="wide selectpicker form-control">
                                        <option selected disabled >Collection Agent</option>
                                        @foreach ($agents as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                        @endforeach
                                        {{-- <option value="xxx">xxx</option>
                                        <option value="yyy">yyy</option> --}}
                                    </select>
                                    {{-- </form> --}}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-raised btn-primary btn-round">Submit</button>
                        </div>
                    </form>


                </div>

            </div>
        </div>

    </div>



    @include('common.javascript')
    <script>
        function assignAgent(id) {
            // console.log(id);
            // var details = JSON.parse(userDetails);

            document.getElementById('assignAgentForm').action = 'asignAgent/' + id;

        }
    </script>
</body>

</html>
