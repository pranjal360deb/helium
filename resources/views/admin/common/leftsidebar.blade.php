<aside id="leftsidebar" class="sidebar">
    <div class="navbar-brand">
        <button class="btn-menu ls-toggle-btn" type="button"><i class="zmdi zmdi-menu"></i></button>
        <a href="{{route('dashboard')}}"><img src="{{asset('assets\images\logo.svg')}}" width="25" alt="Aero"><span class="m-l-10">Aero</span></a>
    </div>
    <div class="menu">
        <ul class="list">
            <li>
                <div class="user-info">
                    <a class="image" href="#"><img src="{{asset('assets\images\profile_av.jpg')}}" alt="User"></a>
                    <div class="detail">
                        <h4>{{Auth::user()->name}}</h4>
                        <small>Super Admin</small>
                    </div>
                </div>
            </li>
            <li {!! (Request::is('dashboard') || Request::is('dashboard/*') ? ' class="active open"' : '') !!}><a href="{{route('dashboard')}}"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a></li>
            <li {!! (Request::is('booking') || Request::is('booking/*') ? ' class="active open"' : '') !!}><a href="{{route('booking')}}"><i class="zmdi zmdi-check-circle"></i><span>Bookings</span></a></li>
            <li {!! (Request::is('hospital') || Request::is('hospital/*') ? ' class="active open"' : '') !!}>
                <a href="{{route('hospital')}}">
                    <i class="zmdi zmdi-hospital"></i><span>Hospitals</span>
                </a>
            </li>
            <li {!! (Request::is('users') || Request::is('users/*') ? ' class="active open"' : '') !!}><a href="{{route('users')}}"><i class="zmdi zmdi-account"></i><span>Users</span></a></li>
            <li {!! (Request::is('agent') || Request::is('agent/*') ? ' class="active open"' : '') !!}><a href="{{route('agent')}}"><i class="zmdi zmdi-accounts-add"></i><span>Collection Agents</span></a></li>
            <li {!! (Request::is('patient') || Request::is('patient/*') ? ' class="active open"' : '') !!}><a href="{{route('patient')}}"><i class="zmdi zmdi-airline-seat-individual-suite"></i><span>Patients</span></a></li>
            <li {!! (Request::is('settings') || Request::is('settings/*') ? ' class="active open"' : '') !!}><a href="{{route('settings')}}"><i class="zmdi zmdi-settings"></i><span>Setting</span></a></li>

        </ul>
    </div>
</aside>
