<!doctype html>
<html class="no-js " lang="en">

    @include('common.head')


<body class="theme-blush">

    <!-- Page Loader -->
    @include('common.pageloader')


    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>

    <!-- Main Search -->
    <div id="search">
        <button id="close" type="button" class="close btn btn-primary btn-icon btn-icon-mini btn-round">x</button>
        <form>
            <input type="search" value="" placeholder="Search..." />
            <button type="submit" class="btn btn-primary">Search</button>
        </form>
    </div>

    <!-- Right Icon menu Sidebar -->
    @include('admin.common.rightsidebar')
    <!-- Left Sidebar -->
    @include('admin.common.leftsidebar')
    {!! Toastr::message() !!}

    <section class="content">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Add Agent</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i>Helium</a></li>
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Add Agent</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <!-- Input -->
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <div class="body">
                                <form action="{{route('creating.agent')}}" method="POST">

                                    @csrf
                                <div class="row clearfix">

                                    <div class="col-sm-12">
                                        <label>Name</label>
                                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                            <input type="text" class="form-control" name="name" placeholder="Agent Name" value="{{ old('name') }}" />
                                            <span class="text-danger">{{ $errors->first('name') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Phone No:</label>
                                        <div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
                                            <input type="text" class="form-control" name="phone" placeholder="Contact No." value="{{ old('phone') }}" />
                                            <span class="text-danger">{{ $errors->first('phone') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Email</label>
                                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                            <input type="text" class="form-control" name="email" placeholder="Email ID" value="{{ old('email') }}" />
                                            <span class="text-danger">{{ $errors->first('email') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <label>Address</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <textarea rows="4" class="form-control no-resize" name="address" placeholder="Address"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-raised btn-primary btn-round waves-effect">Submit</button>
                                    </div>
                                </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </section>

    @include('common.javascript')

</body>

</html>
