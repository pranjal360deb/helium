<!doctype html>
<html class="no-js " lang="en">

    @include('common.head')


<body class="theme-blush">

    @include('common.pageloader')


    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>

    <!-- Main Search -->
    <div id="search">
        <button id="close" type="button" class="close btn btn-primary btn-icon btn-icon-mini btn-round">x</button>
        <form>
            <input type="search" value="" placeholder="Search..." />
            <button type="submit" class="btn btn-primary">Search</button>
        </form>
    </div>

     <!-- Right Icon menu Sidebar -->
     @include('admin.common.rightsidebar')
     <!-- Left Sidebar -->
     @include('admin.common.leftsidebar')
     {!! Toastr::message() !!}

    <section class="content">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Agent Edit</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i>Helium</a></li>
                            <li class="breadcrumb-item active">Agent Edit</li>
                        </ul>
                        <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
                         <a href="{{route('agent')}}" class="btn btn-success btn-icon float-right" type="button">
<!--                        <i class="zmdi zmdi-plus"></i>-->
                     Agent-List</a>

                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="body">
                                <form action="{{route('editing.agent')}}" method="POST">
                                    @csrf
                                <div class="row clearfix">
                                    <input type="text"  name="id" value="{{$id}}"  style="display : none">
                                    <div class="col-lg-12 col-md-12">
                                        <label>Name</label>
                                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                                            <input type="text" class="form-control" value="{{$hosName}}"  name="name" placeholder="Agent Name" >
                                            <span class="text-danger">{{ $errors->first('name') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12">
                                        <label>Phone No:</label>
                                        <div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
                                            <input type="text" class="form-control" value="{{$hosPhoneNo}}" name="phone" placeholder="Contact No">
                                            <span class="text-danger">{{ $errors->first('phone') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-12">
                                        <label>Email</label>
                                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                            <input type="text" class="form-control" value="{{$hosEmail}}" name="email" placeholder="Email Id">
                                            <span class="text-danger">{{ $errors->first('email') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <label>Address</label>
                                        <div class="form-group">
                                            <textarea rows="4" class="form-control no-resize" name="address" placeholder="Address Line 1">{{$hosAddress}}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary">Save Changes</button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('common.javascript')

</body>

</html>
