<!doctype html>
<html class="no-js " lang="en">

    @include('common.head')


<body class="theme-blush">

    <!-- Page Loader -->
    @include('common.pageloader')


    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>


    <!-- Main Search -->
    <div id="search">
        <button id="close" type="button" class="close btn btn-primary btn-icon btn-icon-mini btn-round">x</button>
        <form>
            <input type="search" value="" placeholder="Search..." />
            <button type="submit" class="btn btn-primary">Search</button>
        </form>
    </div>

    <!-- Right Icon menu Sidebar -->
    @include('admin.common.rightsidebar')
    <!-- Left Sidebar -->
   @include('admin.common.leftsidebar')

    <section class="content">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Add Hospital</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i>Helium</a></li>
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Add Hospital</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
                        <a href="{{route('hospital')}}" class="btn btn-success btn-icon float-right" type="button">
<!--                        <i class="zmdi zmdi-plus"></i>-->
                     Hospital List</a>
                    </div>
                </div>
            </div>
            {!! Toastr::message() !!}

            <div class="container-fluid">
                <!-- Input -->
                <div class="row clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="card">
                            <div class="body">
                                <form action="{{route('creating.hospital')}}" method="POST">

                                    @csrf
                                <div class="row clearfix">
                                    <div class="col-sm-6">
                                        <label>Name</label>
                                        <div class="form-group">
                                            <input type="text" name="hospital_name" class="form-control" required placeholder="Hospital Name" value="{{ old('hospital_name') }}" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Phone No</label>
                                        <div class="form-group {{ $errors->has('phone_no') ? 'has-error' : '' }}">
                                            <input type="text"  id="phoneNumber" name="phone_no" class="form-control" required placeholder="Contact No." value="{{ old('phone_no') }}" />
                                            <span class="text-danger">{{ $errors->first('phone_no') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <label>Email Id</label>
                                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                            <input type="text" name="email" class="form-control" placeholder="Email ID" value="{{ old('email') }}" />
                                            <span class="text-danger">{{ $errors->first('email') }}</span>
                                        </div>
                                        {{-- @if($errors->has('email'))
                                            <div class="error">{{ $errors->first('email') }}</div>
                                        @endif --}}
                                    </div>
                                    <div class="col-sm-4">
                                        <label>Registration Number</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="registration_number" required placeholder="Registration No." value="{{ old('registration_number') }}" />
                                        </div>
                                    </div>
                                    {{-- <div class="col-sm-4">
                                        <div class="form-group">
                                            <input type="date" class="form-control" placeholder="Date" />
                                        </div>
                                    </div> --}}
                                    <div class="col-sm-4">
                                        <label>Zip Code</label>
                                        <div class="form-group {{ $errors->has('pin') ? 'has-error' : '' }}">
                                            <input type="number" class="form-control" name="pin"  placeholder="Zip Code" value="{{ old('pin') }}" />
                                            <span class="text-danger">{{ $errors->first('pin') }}</span>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <label>Address</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <textarea rows="4" name="address" class="form-control no-resize" placeholder="Address"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-raised btn-primary btn-round waves-effect">Submit</button>
                                    </div>
                                </div>
                            </form>


                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </section>

    <!-- Jquery Core Js -->
    @include('common.javascript')
    <script>

    </script>
</body>

</html>
