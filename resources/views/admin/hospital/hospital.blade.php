<!doctype html>
<html class="no-js " lang="en">

@include('common.head')

<body class="theme-blush">

    <!-- Page Loader -->
    @include('common.pageloader')

    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>

    <!-- Main Search -->
    <div id="search">
        <button id="close" type="button" class="close btn btn-primary btn-icon btn-icon-mini btn-round">x</button>
        <form>
            <input type="search" value="" placeholder="Search..." />
            <button type="submit" class="btn btn-primary">Search</button>
        </form>
    </div>

    <!-- Right Icon menu Sidebar -->
    @include('admin.common.rightsidebar')
    <!-- Left Sidebar -->
    @include('admin.common.leftsidebar')

    <!-- Main Content -->
    {!! Toastr::message() !!}

    <section class="content">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>Hospital</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i>Helium</a></li>
                        <li class="breadcrumb-item active">Hospital</li>
                    </ul>
                    <button class="btn btn-primary btn-icon mobile_menu" type="button"><i
                            class="zmdi zmdi-sort-amount-desc"></i></button>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i
                            class="zmdi zmdi-arrow-right"></i></button>
                    <a href="{{ route('create.hospital') }}" class="btn btn-success btn-icon float-right" type="button">
                        <!--                        <i class="zmdi zmdi-plus"></i>-->
                        Add Hospital
                    </a>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="body">
                            <div class="row">
                                <div class="col-md-2">
                                    <p>List of Hospital
                                    </p>
                                </div>
                                <div class="col-md-10">
                                    <div class="float-right">

                                        <form action="{{ route('hospital.search') }}" method="post">
                                            @csrf
                                            <ul class="list-inline hospital-filter">
                                                <li class="hospital-filter1">
                                                    <!-- Search form -->
                                                    <input class="form-control search" name="name" type="text"
                                                        placeholder="Search" aria-label="Search">
                                                </li>
                                                <li class="hospital-filter1">
                                                    <div class="box1">
                                                        <select name="status" class="wide selectpicker form-control">
                                                            <option value="" selected disabled>Select Status</option>
                                                            <option value='1'>Active</option>
                                                            <option value='0'>Inactive</option>
                                                        </select>
                                                    </div>
                                                </li>
                                                <li class="hospital-filter1">
                                                    <button type="submit"
                                                        class="btn btn-raised btn-primary btn-round">Search</button>
                                                </li>
                                            </ul>
                                        </form>
                                    </div>

                                </div>

                            </div>
                            <div class="table-responsive">
                                <table class="table  table-striped">
                                    <thead>
                                        <tr>
                                            <th style="width:5%;">Sl No.</th>
                                            <th style="width:20%;">Hospital name</th>
                                            <th style="width:15%;">Zip Code</th>
                                            <th style="width:15%;">Email id</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($hospitalLists as $index => $item)
                                            <tr>
                                                <td>{{ $index + 1 }}</td>
                                                <td>{{ $item->hospitalDetails->hospital_name }}</td>
                                                <td>{{ $item->hospitalDetails->pin ?? '--' }}</td>
                                                <td>{{ $item->email ?? '--' }}</td>
                                                <td>
                                                    {{-- <button
                                                        onclick="check()"></button> --}}
                                                    {{-- <div class="active-btn">
                                                        <input onclick="check()" class="checkbox chckBtn" id="checkbox1"
                                                            type="checkbox" />
                                                        <label class="checkbox-label">
                                                            <span class="on ">Active</span>
                                                            <span class="off">Deactive</span>
                                                        </label>
                                                        onChange="handleCheckboxChange(this,'{{ route('active', ['id' => $item->id]) }}'
                                                        )"
                                                    </div> --}}

                                                    @if ($item->is_deactivate == 1)
                                                    <label class="switch">
                                                        <input id="testingUpdate" data-id="{{ $item->id }}"
                                                            class="toggle-deactive" type="checkbox" checked>
                                                        <span class="slider round">

                                                        </span>
                                                    </label>
                                                        @else
                                                        <label class="switch">
                                                            <input id="testingUpdate" data-id="{{ $item->id }}"
                                                                class="toggle-deactive" type="checkbox" >
                                                            <span class="slider round">

                                                            </span>
                                                        </label>

                                                    @endif



                                                </td>
                                                <td>
                                                    {{-- <ul
                                                        class="list-inline hospital-action">
                                                        <li> --}}
                                                            <a href="{{ route('view.hospital', ['id' => $item->id]) }}"
                                                                class="btn btn-info">View</a>
                                                            {{--
                                                        </li>
                                                        <li> --}}
                                                            <button onclick="deleteHospital('{{ $item->id }}')"
                                                                data-toggle="modal" data-target="#smallModal"
                                                                class="btn btn-danger">Remove</button>
                                                            {{--
                                                        </li>
                                                    </ul> --}}
                                                </td>
                                            </tr>

                                        @endforeach
                                    </tbody>
                                </table>

                                <div class="d-flex justify-content-end">
                                    {{ $hospitalLists->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>






        </div>
    </section>

    <div class="modal fade" id="smallModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <h5 class="modal-heading">Are You Sure ?</h5>
                    <p>Do you really want to delete these records? This process cannot be undone.</p>
                    <div>
                        <form method="POST" id="deleteUserForm">
                            @csrf
                            <ul class="list-inline modal-des">

                                <li><button type="submit" class="btn btn-info">Yes</button></li>
                                <li><button type="button" class="btn btn-danger" data-dismiss="modal">No</button></li>
                            </ul>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    @include('common.javascript')
    <script>
        $(document.body).on('change', '#testingUpdate', function() {
            var status = $(this).prop('checked') == true ? 1 : 0;
            var user_id = $(this).data('id');
            console.log(status);
            var formDat = {
                id: user_id,
                value: status
            }
            // console.log(formDat);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "post",
                // dataType: "json",
                // _token: "{{ csrf_token() }}",
                url: '/active',
                // data: {'status': status, 'user_id': user_id},
                data: formDat,
                success: function(data) {
                    console.log(data)
                }
            });
            // alert(user_id);
        });
        // $(function() {
        //     $('.toggle-active').change(function() {
        //         var status = $(this).prop('checked') == true ? 1 : 0;
        //         var user_id = $(this).data('id');
        //         console.log(status);
        //         var formDat = {
        //             id: user_id,
        //             value: status
        //         }
        //         // console.log(formDat);
        //         $.ajax({
        //             headers: {
        //                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //             },
        //             type: "post",
        //             // dataType: "json",
        //             // _token: "{{ csrf_token() }}",
        //             url: '/active',
        //             // data: {'status': status, 'user_id': user_id},
        //             data: formDat,
        //             success: function(data) {
        //                 console.log(data)
        //             }
        //         });
        //     })

        //     $('.toggle-deactive').change(function() {
        //         var status = $(this).prop('checked') == true ? 1 : 0;
        //         var user_id = $(this).data('id');
        //         console.log(status);
        //         var formDat = {
        //             id: user_id,
        //             value: status
        //         }
        //         // console.log(formDat);

        //         $.ajax({
        //             headers: {
        //                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //             },
        //             type: "post",
        //             // dataType: "json",
        //             // _token: "{{ csrf_token() }}",
        //             url: '/active',
        //             // data: {'status': status, 'user_id': user_id},
        //             data: formDat,
        //             success: function(data) {
        //                 console.log(data)
        //             }
        //         });
        //     })
        // })


        function deleteHospital(id) {
            // var details = JSON.parse(userDetails);

            document.getElementById('deleteUserForm').action = 'deleteHospital/' + id;

        }

    </script>

</body>

</html>
