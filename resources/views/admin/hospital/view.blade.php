<!doctype html>
<html class="no-js " lang="en">

@include('common.head')

<body class="theme-blush">

    <!-- Page Loader -->
    @include('common.pageloader')


    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>

    <!-- Main Search -->
    <div id="search">
        <button id="close" type="button" class="close btn btn-primary btn-icon btn-icon-mini btn-round">x</button>
        <form>
            <input type="search" value="" placeholder="Search..." />
            <button type="submit" class="btn btn-primary">Search</button>
        </form>
    </div>

     <!-- Right Icon menu Sidebar -->
     @include('admin.common.rightsidebar')
     <!-- Left Sidebar -->
    @include('admin.common.leftsidebar')


    <section class="content">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Hospital Profile</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Helium</a></li>
                            <li class="breadcrumb-item active">Hospital Profile</li>
                        </ul>
                        <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
                        <a href="{{route('edit.hospital',['id'=>$id])}}" class="btn btn-info btn-icon float-right">
                            Edit
<!--                            <i class="zmdi zmdi-edit"></i>-->
                        </a>
                        <a href="{{route('hospital')}}" class="btn btn-success btn-icon float-right" type="button">
<!--                        <i class="zmdi zmdi-plus"></i>-->
                     Hospital List</a>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row clearfix">

                    <div class="col-lg-12 col-md-12">
                        <div class="row clearfix">
                            <div class="col-md-12 col-sm-12">
                                <div class="card">
                                    <div class="body">
                                        <!--
                                        <a href="https://themeforest.net/item/arrowlite-responsive-admin-dashboard-template/23656497">
                                            <img class="img-fluid img-thumbnail" src="http://www.wrraptheme.com//templates/preview/arrowlite.png" alt="">
                                        </a>
-->
                                        <div class="row">
                                            <div class="col-md-10">
                                                <h5 class="mt-3"><b>{{$hosName}}</b></h5>
                                            </div>
                                            <div class="col-md-2">
                                                {{-- <div>
                                                    <input class="checkbox chckBtn" id="checkbox1" type="checkbox" />
                                                    <label for="checkbox1" class="checkbox-label">
                                                        <span class="on ">Active</span>
                                                        <span class="off">Deactive</span>
                                                    </label>
                                                </div> --}}
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Phone No.</label>
                                                <p>{{$hosPhoneNo ?? '--'}}</p>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Email Id</label>
                                                <p>{{$hosEmail ?? '--'}}</p>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Registration No.</label>
                                                <p>{{$hosRegistration ?? '--'}}</p>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Date</label>
                                                <p>{{$hosDateRegis ?? '--'}}</p>
                                            </div>
                                             <div class="col-md-6">
                                                <label>Zip Code</label>
                                                <p>{{$hosPin ?? '--'}}</p>
                                            </div>


                                            <div class="col-md-12">
                                                <label>Address</label>
                                                <p>{{$hosAddress ?? '--'}}</p>
                                            </div>
                                        </div>
                                        {{-- <div class="justify-content-between">
                                            <a href="booking.html" class="btn btn-info">View Bookings</a>
                                            <a href="#" class="btn btn-danger">Remove</a>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Jquery Core Js -->
   @include('common.javascript')
</body>

</html>
