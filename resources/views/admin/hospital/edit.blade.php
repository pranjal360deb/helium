<!doctype html>
<html class="no-js " lang="en">

    @include('common.head')


<body class="theme-blush">

    <!-- Page Loader -->
    @include('common.pageloader')


    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>

    <!-- Main Search -->
    <div id="search">
        <button id="close" type="button" class="close btn btn-primary btn-icon btn-icon-mini btn-round">x</button>
        <form>
            <input type="search" value="" placeholder="Search..." />
            <button type="submit" class="btn btn-primary">Search</button>
        </form>
    </div>

<!-- Right Icon menu Sidebar -->
@include('admin.common.rightsidebar')
<!-- Left Sidebar -->
@include('admin.common.leftsidebar')

    <section class="content">
        <div class="body_scroll">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12">
                        <h2>Hospital Edit</h2>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i>Helium</a></li>
                            <li class="breadcrumb-item active">Hospital Edit</li>
                        </ul>
                        <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i class="zmdi zmdi-arrow-right"></i></button>
                         <a href="{{route('hospital')}}" class="btn btn-success btn-icon float-right" type="button">
<!--                        <i class="zmdi zmdi-plus"></i>-->
                     Hospital List</a>

                    </div>
                </div>
            </div>
            {!! Toastr::message() !!}
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="body">
                                <form action="{{route('editing.hospital')}}" method="POST">

                                    @csrf
                                <div class="row clearfix">
                                    <input type="text"  name="id" value="{{$id}}" style="display : none">
                                     <div class="col-sm-6">
                                        <label>Name</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="hospital_name" value="{{$hosName}}" placeholder="Hospital Name" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Phone No</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="phone_no" value="{{$hosPhoneNo}}" placeholder="Contact No." />
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <label>Email Id</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="email" value="{{$hosEmail}}" disabled placeholder="Email ID" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <label>Registration Number</label>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="registration_no" value="{{$hosRegistration}}" placeholder="Registration No." />
                                        </div>
                                    </div>
                                    {{-- <div class="col-sm-4">
                                        <div class="form-group">
                                            <input type="date" class="form-control" placeholder="Date" />
                                        </div>
                                    </div> --}}
                                    <div class="col-sm-4">
                                        <label>Zip Code</label>
                                        <div class="form-group">
                                            <input type="text" name="pin" class="form-control" value="{{$hosPin}}" placeholder="Zip Code" />
                                        </div>
                                    </div>
                                    {{-- <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Status" />
                                        </div>
                                    </div> --}}
                                    <div class="col-sm-12">
                                        <label>Address</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <textarea rows="4" name="address" class="form-control no-resize"  placeholder="Address">{{$hosAddress}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary">Save Changes</button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Jquery Core Js -->
    @include('common.javascript')
</body>

</html>
