<!doctype html>
<html class="no-js " lang="en">

@include('common.head')

<body class="theme-blush">

    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/loader.svg" width="48" height="48"
                    alt="Aero"></div>
            <p>Please wait...</p>
        </div>
    </div>

    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>

    <!-- Main Search -->
    <div id="search">
        <button id="close" type="button" class="close btn btn-primary btn-icon btn-icon-mini btn-round">x</button>
        <form>
            <input type="search" value="" placeholder="Search..." />
            <button type="submit" class="btn btn-primary">Search</button>
        </form>
    </div>

    <!-- Right Icon menu Sidebar -->
    <div class="navbar-right">
        <ul class="navbar-nav">
            <li><a href="#search" class="main_search" title="Search..."><i class="zmdi zmdi-search"></i></a></li>
            <li class="dropdown">
                <a href="javascript:void(0);" class="dropdown-toggle" title="Notifications" data-toggle="dropdown"
                    role="button"><i class="zmdi zmdi-notifications"></i>
                    <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
                </a>
                <ul class="dropdown-menu slideUp2">
                    <li class="header">Notifications</li>
                    <li class="body">
                        <ul class="menu list-unstyled">
                            <li>
                                <a href="javascript:void(0);">
                                    <div class="icon-circle bg-blue"><i class="zmdi zmdi-account"></i></div>
                                    <div class="menu-info">
                                        <h4>8 New Members joined</h4>
                                        <p><i class="zmdi zmdi-time"></i> 14 mins ago </p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">
                                    <div class="icon-circle bg-amber"><i class="zmdi zmdi-shopping-cart"></i></div>
                                    <div class="menu-info">
                                        <h4>4 Sales made</h4>
                                        <p><i class="zmdi zmdi-time"></i> 22 mins ago </p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">
                                    <div class="icon-circle bg-red"><i class="zmdi zmdi-delete"></i></div>
                                    <div class="menu-info">
                                        <h4><b>Nancy Doe</b> Deleted account</h4>
                                        <p><i class="zmdi zmdi-time"></i> 3 hours ago </p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">
                                    <div class="icon-circle bg-green"><i class="zmdi zmdi-edit"></i></div>
                                    <div class="menu-info">
                                        <h4><b>Nancy</b> Changed name</h4>
                                        <p><i class="zmdi zmdi-time"></i> 2 hours ago </p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">
                                    <div class="icon-circle bg-grey"><i class="zmdi zmdi-comment-text"></i></div>
                                    <div class="menu-info">
                                        <h4><b>John</b> Commented your post</h4>
                                        <p><i class="zmdi zmdi-time"></i> 4 hours ago </p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">
                                    <div class="icon-circle bg-purple"><i class="zmdi zmdi-refresh"></i></div>
                                    <div class="menu-info">
                                        <h4><b>John</b> Updated status</h4>
                                        <p><i class="zmdi zmdi-time"></i> 3 hours ago </p>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);">
                                    <div class="icon-circle bg-light-blue"><i class="zmdi zmdi-settings"></i></div>
                                    <div class="menu-info">
                                        <h4>Settings Updated</h4>
                                        <p><i class="zmdi zmdi-time"></i> Yesterday </p>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="footer"> <a href="javascript:void(0);">View All Notifications</a> </li>
                </ul>
            </li>
            <li><a href="sign-in.html" class="mega-menu" title="Sign Out"><i class="zmdi zmdi-power"></i></a></li>
        </ul>
    </div>

    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <div class="navbar-brand">
            <button class="btn-menu ls-toggle-btn" type="button"><i class="zmdi zmdi-menu"></i></button>
            <a href="index.html"><img src="assets/images/logo.svg" width="25" alt="Aero"><span
                    class="m-l-10">Aero</span></a>
        </div>
        <div class="menu">
            <ul class="list">
                <!--
                <li>
                    <div class="user-info">
                        <a class="image" href="profile.html"><img src="assets/images/profile_av.jpg" alt="User"></a>
                        <div class="detail">
                            <h4>Michael</h4>
                            <small>Super Admin</small>
                        </div>
                    </div>
                </li>
-->
                <li class="active open"><a href="index.html"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a>
                </li>
                <li><a href="booking.html"><i class="zmdi zmdi-home"></i><span>Bookings</span></a></li>
                <li><a href="hospital.html"><i class="zmdi zmdi-home"></i><span>Hospital</span></a></li>
                <li><a href="user.html"><i class="zmdi zmdi-home"></i><span>User</span></a></li>
                <li><a href="collection-agent.html"><i class="zmdi zmdi-home"></i><span>Collection Agent</span></a></li>
                <li><a href="setting.html"><i class="zmdi zmdi-home"></i><span>Setting</span></a></li>
                <li><a href="report.html"><i class="zmdi zmdi-home"></i><span>Reports</span></a></li>

            </ul>
        </div>
    </aside>


    <!-- Main Content -->

    <section class="content">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>Dashboard</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i>Helium</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ul>
                    <button class="btn btn-primary btn-icon mobile_menu" type="button"><i
                            class="zmdi zmdi-sort-amount-desc"></i></button>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <button class="btn btn-primary btn-icon float-right right_icon_toggle_btn" type="button"><i
                            class="zmdi zmdi-arrow-right"></i></button>
                </div>
            </div>
        </div>
        <div class="container-fluid">


            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="body">
                            <div class="row">
                                <div class="col-md-10">
                                    <p>List of Bookings
                                    </p>
                                </div>
                                <div class="col-md-2">
                                    <div class="mb-3">
                                        <select class="form-control show-tick">
                                            <option>Today</option>
                                            <option>Tomorrow</option>
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Booking Id</th>
                                            <th>Name</th>
                                            <th>Boking Date &amp; time</th>
                                            <th>Schedule Date &amp; time</th>
                                            <th>Status</th>
                                            <th>Ph No.</th>
                                            <th>Member</th>
                                            <th>Test Type</th>

                                            <th>Zip Code</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row">1</th>
                                            <td>Debabrat Sharma</td>
                                            <td>5/10/2020 3:20 PM</td>
                                            <td>7/10/2020 3:20 PM</td>
                                            <td>Scheduled</td>
                                            <td>8136081360</td>
                                            <td>2</td>
                                            <td>Rapid Antigen test</td>
                                            <td>781127</td>
                                            <td><a href="booking-details.html" class="btn btn-info">View</a></td>
                                        </tr>
                                        <tr>
                                            <th scope="row">1</th>
                                            <td>Debabrat Sharma</td>
                                            <td>5/10/2020 3:20 PM</td>
                                            <td>7/10/2020 3:20 PM</td>
                                            <td>Scheduled</td>
                                            <td>8136081360</td>
                                            <td>2</td>
                                            <td>Rapid Antigen test</td>
                                            <td>781127</td>
                                            <td><a href="booking-details.html" class="btn btn-info">View</a></td>
                                        </tr>
                                        <tr>
                                            <th scope="row">1</th>
                                            <td>Debabrat Sharma</td>
                                            <td>5/10/2020 3:20 PM</td>
                                            <td>7/10/2020 3:20 PM</td>
                                            <td>Scheduled</td>
                                            <td>8136081360</td>
                                            <td>2</td>
                                            <td>Rapid Antigen test</td>
                                            <td>781127</td>
                                            <td><a href="booking-details.html" class="btn btn-info">View</a></td>
                                        </tr>
                                        <tr>
                                            <th scope="row">1</th>
                                            <td>Debabrat Sharma</td>
                                            <td>5/10/2020 3:20 PM</td>
                                            <td>7/10/2020 3:20 PM</td>
                                            <td>Scheduled</td>
                                            <td>8136081360</td>
                                            <td>2</td>
                                            <td>Rapid Antigen test</td>
                                            <td>781127</td>
                                            <td><a href="booking-details.html" class="btn btn-info">View</a></td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                            <div class="row">
                                <div class="col-md-12"><a href="booking.html" class="btn btn-info">View All</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="body">

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs p-0 mb-3">
                                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#home">Weekly
                                        Booking Record</a></li>
                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#profile">Monthly
                                        Booking Record</a></li>
                                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#messages">Yearly
                                        Booking Record</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane in active" id="home">
                                    <b>Weekly Booking Record</b>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <canvas id="myChart"></canvas>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="profile">
                                    <b>Monthly Booking Record</b>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <canvas id="myChart1"></canvas>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="messages">
                                    <b>Yearly Booking Record</b>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <canvas id="myChart2"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </section>


    <!-- Jquery Core Js -->
    @include('common.javascript')

    <script>
        var ctx = document.getElementById("myChart").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
                datasets: [{
                    label: 'bookings',
                    data: [12, 19, 3, 17, 28, 24, 7],
                    backgroundColor: "rgb(228 114 151 / 89%)"
                }]
            }
        });

    </script>

    <script>
        var ctx = document.getElementById("myChart1").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                datasets: [{
                    label: 'bookings',
                    data: [12, 19, 3, 17, 28, 24, 7, 15, 20, 14, 16, 10],
                    backgroundColor: "rgb(228 114 151 / 89%)"
                }]
            }
        });

    </script>

    <script>
        var ctx = document.getElementById("myChart2").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                datasets: [{
                    label: 'bookings',
                    data: [12, 19, 3, 17, 28, 24, 7, 15, 20, 14, 16, 10],
                    backgroundColor: "rgb(228 114 151 / 89%)"
                }]
            }
        });

    </script>



</body>

</html>
