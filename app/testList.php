<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class testList extends Model
{
    //
    use SoftDeletes;

    protected $table = 'booking';
    protected $guarded = [];
    protected $dates = ['deleted_at'];
}
