<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hospital extends Model
{
    //
    use SoftDeletes;

    protected $table = 'hospital';
    // protected $fillable = ['name', 'detail'];
    protected $guarded = [];
    protected $dates = ['deleted_at'];
}
