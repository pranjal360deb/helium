<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserDetails extends Model
{
    //
    use SoftDeletes;

    protected $table = 'usersdetails';
    // protected $fillable = ['name', 'detail'];
    protected $guarded = [];
    protected $dates = ['deleted_at'];

    public function userList() {
        return $this->belongsTo(User::class,'user_id','id');
    }

}
