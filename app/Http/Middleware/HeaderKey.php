<?php

namespace App\Http\Middleware;

use Closure;

class HeaderKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $hostname = env("key");
        if ($request->header('key') != $hostname) {
            return response()->json(['success'=>false]);
        }
        return $next($request);
    }
}
