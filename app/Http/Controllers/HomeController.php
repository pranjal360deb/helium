<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function redirect()
    {
        // if (! auth()->check() ) {
        //     return redirect()->to( '/auth/login' );
        // }

        if (Auth::user()->role_id == '1') {
            // dd('hi admin');
            // return view('');
            return redirect()->to( '/dashboard' );
            // return redirect()->to( '/admin-dashboard' );
        }
         if(Auth::user()->role_id == '2'){
                            //  dd(Auth::user()->is_deactivate);
             if(Auth::user()->is_deactivate == 1) {
                // dd(Auth::user()->is_deactivate);
              return redirect()->to( '/hospitalPanel/dashboard' );
             }
             else {
                return redirect()->to( '/' );
             }

            // return redirect()->to( '/hospital/dashboard' );

        }
        // else {
            // dd('not admin');
            return redirect()->to( '/' );

        // }

        // return redirect()->to( '/dashboard' );
    }
}
