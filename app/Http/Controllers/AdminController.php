<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Booking;
use App\User;
use Carbon\Carbon;

class AdminController extends Controller
{

    public function dashboard()
    {
        $booking = Booking::get();
        $bookingCount = $booking->count();
        $bookingTomorrow = Booking::whereDate('booking_date', Carbon::tomorrow())->get();
        $bookingToday = Booking::whereDate('booking_date', Carbon::today())->get();
        $user = User::where('role_id', 3)->get();
        $userCount = $user->count();
        $userToday = User::where('role_id', 3)->whereDate('created_at', Carbon::today())->get();
        $userCountToday = $userToday->count();

        $hospitalToday = User::where('role_id', 2)->whereDate('created_at', Carbon::today())->get();
        $hospitalCountToday = $hospitalToday->count();

        $hospital = User::where('role_id', 2)->get();
        $hospitalCount = $hospital->count();
        $agent = User::where('role_id', 4)->get();
        $agentCount = $agent->count();

        $bookings = Booking::with(['userList' => function ($e) {
            return $e->with('userDetails');
        }])
            ->with(['hospitalList' => function ($e) {
                return $e->with('hospitalDetails');
            }])
            ->get();

        $bookingSaturday = Booking::get();
        $filteredArray = array();
        $fridayArray = array();
        $thursdayArray = array();
        $wednesdayArray = array();
        $tuesdayArray = array();
        $mondayArray = array();

        $firstYearArray = array();
        $thirdYearArray = array();
        $secYearArray = array();
        $fourthYearArray = array();
        $fifthYearArray = array();
        $sixthYearArray = array();

        //yearly
        $januaryArray = array();
        $febArray = array();
        $marArray = array();
        $aprArray = array();
        $mayArray = array();
        $juneArray = array();
        $julyArray = array();
        $augustArray = array();
        $septArray = array();
        $octoberArray = array();
        $novArray = array();
        $decArray = array();



        foreach ($bookingSaturday as $record) {
            //weekly
            if (Carbon::parse($record->created_at)->dayOfWeek == 6) {
                $filteredArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->dayOfWeek == 5) {
                $fridayArray[] = $record;
            }

            if (Carbon::parse($record->created_at)->dayOfWeek == 4) {
                $thursdayArray[] = $record;
            }

            if (Carbon::parse($record->created_at)->dayOfWeek == 3) {
                $wednesdayArray[] = $record;
            }

            if (Carbon::parse($record->created_at)->dayOfWeek == 2) {
                $tuesdayArray[] = $record;
            }

            if (Carbon::parse($record->created_at)->dayOfWeek == 1) {
                $mondayArray[] = $record;
            }

            //yearly
            if (Carbon::parse($record->created_at)->year == 2020) {
                $firstYearArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->year == 2021) {
                $secYearArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->year == 2022) {
                $thirdYearArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->year == 2023) {
                $fourthYearArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->year == 2024) {
                $fifthYearArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->year == 2025) {
                $sixthYearArray[] = $record;
            }

            //monthly
            if (Carbon::parse($record->created_at)->month == 01) {
                $januaryArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 02) {
                $febArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 03) {
                $marArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 04) {
                $aprArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 05) {
                $mayArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 06) {
                $juneArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 07) {
                $julyArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == '08') {
                $augustArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == '09') {
                $septArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 10) {
                $octoberArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 11) {
                $novArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 12) {
                $decArray[] = $record;
            }
        }
        //weekly
        $bookingSaturdayCount = sizeof($filteredArray);
        $bookingFridayCount = sizeof($fridayArray);
        $bookingThrusdayCount = sizeof($thursdayArray);
        $bookingWednesdayCount = sizeof($wednesdayArray);
        $bookingTuesdayCount = sizeof($tuesdayArray);
        $bookingMondayCount = sizeof($mondayArray);

        //yearly
        $firstYearArrayCount = sizeof($firstYearArray);
        $secYearArrayCount = sizeof($secYearArray);
        $thirdYearArrayCount = sizeof($thirdYearArray);
        $fourthYearArrayCount = sizeof($fourthYearArray);
        $fifthYearArrayCount = sizeof($fifthYearArray);
        $sixthYearArrayCount = sizeof($sixthYearArray);

        //monthly
        $januaryArrayCount = sizeof($januaryArray);
        $febArrayCount = sizeof($febArray);
        $marArrayCount = sizeof($marArray);
        $aprArrayCount = sizeof($aprArray);
        $mayArrayCount = sizeof($mayArray);
        $juneArrayCount = sizeof($juneArray);
        $julyArrayCount = sizeof($julyArray);
        $augustArrayCount = sizeof($augustArray);
        $septArrayCount = sizeof($septArray);
        $octoberArrayCount = sizeof($octoberArray);
        $novArrayCount = sizeof($novArray);
        $decArrayCount = sizeof($decArray);



        $bookingSunday = Booking::get();
        $sundayArray = array();
        foreach ($bookingSunday as $record) {
            if (Carbon::parse($record->created_at)->dayOfWeek == 0) {
                $sundayArray[] = $record;
            }
        }
        $bookingSundayCount = sizeof($sundayArray);


        $allBook = Booking::with(['userList' => function ($e) {
            return $e->with('userDetails');
        }])
            ->with(['hospitalList' => function ($e) {
                return $e->with('hospitalDetails');
            }])
            ->paginate(10);


        // $monday = whereRaw('WEEKDAY(booking.created_at) = 1')->get();

        // dd($bookingSundayCount);
        // return \response()->json($details);

        return view('admin.dashboard.dashboard', compact(
            'userCount',
            'hospitalCount',
            'agentCount',
            'bookingCount',
            'userCountToday',
            'hospitalCountToday',
            'bookings',
            'bookingSaturdayCount',
            'bookingSundayCount',
            'bookingFridayCount',
            'bookingThrusdayCount',
            'bookingMondayCount',
            'bookingTuesdayCount',
            'bookingWednesdayCount',
            'firstYearArrayCount',
            'secYearArrayCount',
            'thirdYearArrayCount',
            'fourthYearArrayCount',
            'fifthYearArrayCount',
            'sixthYearArrayCount',

            'octoberArrayCount',
            'januaryArrayCount',
            'febArrayCount',
            'marArrayCount',
            'aprArrayCount',
            'mayArrayCount',
            'juneArrayCount',
            'julyArrayCount',
            'augustArrayCount',
            'septArrayCount',
            'decArrayCount',
            'novArrayCount',
            'allBook'


        ));
        // return response()->json([$bookingCount,$bookingTomorrow,$bookingToday,$userCount,$hospitalCount,$agentCount]);


    }

    public function filterDate(Request $request) {
        // dd($request->day);
        if($request->day == 1)

       {
           $allBook = Booking::whereDate('booking_date', Carbon::today())->with(['userList' => function ($e) {
            return $e->with('userDetails');
            }])
            ->with(['hospitalList' => function ($e) {
                return $e->with('hospitalDetails');
            }])
            ->paginate(10);
        }
        if($request->day == 2) {

            $allBook = Booking::whereDate('booking_date', Carbon::tomorrow())->with(['userList' => function ($e) {
                return $e->with('userDetails');
                }])
                ->with(['hospitalList' => function ($e) {
                    return $e->with('hospitalDetails');
                }])
                ->paginate(10);
            }

        if($request->day == 3) {

            $allBook = Booking::with(['userList' => function ($e) {
                return $e->with('userDetails');
                }])
                ->with(['hospitalList' => function ($e) {
                    return $e->with('hospitalDetails');
                }])
                ->paginate(10);
            }



        $booking = Booking::get();
        $bookingCount = $booking->count();
        $bookingTomorrow = Booking::whereDate('booking_date', Carbon::tomorrow())->get();
        $bookingToday = Booking::whereDate('booking_date', Carbon::today())->get();
        $user = User::where('role_id', 3)->get();
        $userCount = $user->count();
        $userToday = User::where('role_id', 3)->whereDate('created_at', Carbon::today())->get();
        $userCountToday = $userToday->count();

        $hospitalToday = User::where('role_id', 2)->whereDate('created_at', Carbon::today())->get();
        $hospitalCountToday = $hospitalToday->count();

        $hospital = User::where('role_id', 2)->get();
        $hospitalCount = $hospital->count();
        $agent = User::where('role_id', 4)->get();
        $agentCount = $agent->count();

        $bookings = Booking::with(['userList' => function ($e) {
            return $e->with('userDetails');
        }])
            ->with(['hospitalList' => function ($e) {
                return $e->with('hospitalDetails');
            }])
            ->get();

        $bookingSaturday = Booking::get();
        $filteredArray = array();
        $fridayArray = array();
        $thursdayArray = array();
        $wednesdayArray = array();
        $tuesdayArray = array();
        $mondayArray = array();

        $firstYearArray = array();
        $thirdYearArray = array();
        $secYearArray = array();
        $fourthYearArray = array();
        $fifthYearArray = array();
        $sixthYearArray = array();

        //yearly
        $januaryArray = array();
        $febArray = array();
        $marArray = array();
        $aprArray = array();
        $mayArray = array();
        $juneArray = array();
        $julyArray = array();
        $augustArray = array();
        $septArray = array();
        $octoberArray = array();
        $novArray = array();
        $decArray = array();



        foreach ($bookingSaturday as $record) {
            //weekly
            if (Carbon::parse($record->created_at)->dayOfWeek == 6) {
                $filteredArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->dayOfWeek == 5) {
                $fridayArray[] = $record;
            }

            if (Carbon::parse($record->created_at)->dayOfWeek == 4) {
                $thursdayArray[] = $record;
            }

            if (Carbon::parse($record->created_at)->dayOfWeek == 3) {
                $wednesdayArray[] = $record;
            }

            if (Carbon::parse($record->created_at)->dayOfWeek == 2) {
                $tuesdayArray[] = $record;
            }

            if (Carbon::parse($record->created_at)->dayOfWeek == 1) {
                $mondayArray[] = $record;
            }

            //yearly
            if (Carbon::parse($record->created_at)->year == 2020) {
                $firstYearArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->year == 2021) {
                $secYearArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->year == 2022) {
                $thirdYearArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->year == 2023) {
                $fourthYearArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->year == 2024) {
                $fifthYearArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->year == 2025) {
                $sixthYearArray[] = $record;
            }

            //monthly
            if (Carbon::parse($record->created_at)->month == 01) {
                $januaryArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 02) {
                $febArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 03) {
                $marArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 04) {
                $aprArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 05) {
                $mayArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 06) {
                $juneArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 07) {
                $julyArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == '08') {
                $augustArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == '09') {
                $septArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 10) {
                $octoberArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 11) {
                $novArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 12) {
                $decArray[] = $record;
            }
        }
        //weekly
        $bookingSaturdayCount = sizeof($filteredArray);
        $bookingFridayCount = sizeof($fridayArray);
        $bookingThrusdayCount = sizeof($thursdayArray);
        $bookingWednesdayCount = sizeof($wednesdayArray);
        $bookingTuesdayCount = sizeof($tuesdayArray);
        $bookingMondayCount = sizeof($mondayArray);

        //yearly
        $firstYearArrayCount = sizeof($firstYearArray);
        $secYearArrayCount = sizeof($secYearArray);
        $thirdYearArrayCount = sizeof($thirdYearArray);
        $fourthYearArrayCount = sizeof($fourthYearArray);
        $fifthYearArrayCount = sizeof($fifthYearArray);
        $sixthYearArrayCount = sizeof($sixthYearArray);

        //monthly
        $januaryArrayCount = sizeof($januaryArray);
        $febArrayCount = sizeof($febArray);
        $marArrayCount = sizeof($marArray);
        $aprArrayCount = sizeof($aprArray);
        $mayArrayCount = sizeof($mayArray);
        $juneArrayCount = sizeof($juneArray);
        $julyArrayCount = sizeof($julyArray);
        $augustArrayCount = sizeof($augustArray);
        $septArrayCount = sizeof($septArray);
        $octoberArrayCount = sizeof($octoberArray);
        $novArrayCount = sizeof($novArray);
        $decArrayCount = sizeof($decArray);



        $bookingSunday = Booking::get();
        $sundayArray = array();
        foreach ($bookingSunday as $record) {
            if (Carbon::parse($record->created_at)->dayOfWeek == 0) {
                $sundayArray[] = $record;
            }
        }
        $bookingSundayCount = sizeof($sundayArray);





        // $monday = whereRaw('WEEKDAY(booking.created_at) = 1')->get();

        // dd($bookingSundayCount);
        // return \response()->json($details);

        return view('admin.dashboard.dashboard', compact(
            'userCount',
            'hospitalCount',
            'agentCount',
            'bookingCount',
            'userCountToday',
            'hospitalCountToday',
            'bookings',
            'bookingSaturdayCount',
            'bookingSundayCount',
            'bookingFridayCount',
            'bookingThrusdayCount',
            'bookingMondayCount',
            'bookingTuesdayCount',
            'bookingWednesdayCount',
            'firstYearArrayCount',
            'secYearArrayCount',
            'thirdYearArrayCount',
            'fourthYearArrayCount',
            'fifthYearArrayCount',
            'sixthYearArrayCount',

            'octoberArrayCount',
            'januaryArrayCount',
            'febArrayCount',
            'marArrayCount',
            'aprArrayCount',
            'mayArrayCount',
            'juneArrayCount',
            'julyArrayCount',
            'augustArrayCount',
            'septArrayCount',
            'decArrayCount',
            'novArrayCount',
            'allBook'


        ));
    }





    public function getUsers()
    {
        $users = User::where('role_id', 3)->with('userDetails')->paginate(10);
        return view('admin.users.users', compact('users'));
    }

    public function search(Request $request)
    {

        $users = User::where('role_id', 3)
            ->where('is_deactivate', 'like', '%' . $request->status . '%')
            ->where(function ($q) use ($request) {
                $q->where('name', 'like', '%' . $request->name . '%')
                    ->orWhere('email', 'like', '%' . $request->name . '%')
                    ->orWhere('phone_no', 'like', '%' . $request->name . '%')
                    // ->orWhere('is_deactivate', 'like' , '%'.$request->status.'%')
                    ->orWhereHas('userDetails', function ($query) use ($request) {
                        $query->where('pin', 'like', '%' . $request->name . '%')
                            ->orWhere('address', 'like', '%' . $request->name . '%');
                        // ->orWhere('is_deactivate', 'like' , '%'.$request->name.'%');
                    });
            })
            ->with('userDetails')
            ->orderBy('id', 'DESC')
            ->paginate(1);

        // dd($users);
        return view('admin.users.users', compact('users'));
    }

    public function viewUsers($id)
    {

        $hospitalLists = User::with('userDetails')
            ->where('id', $id)
            ->where('role_id', 3)
            ->orderBy('id', 'DESC')
            ->get();

        foreach ($hospitalLists as $item) {

            $hosName = $item->name;
            // $hosRegistration = $item->userDetails->registration_number;
            $hosAddress = $item->userDetails->address;
            $hosEmail = $item->email;
            $hosPin = $item->userDetails->pin;
            $hosPhoneNo = $item->phone_no;
            $hosDateRegis = $item->created_at;
        }
        // dd($hosid);

        return  view('admin.users.view', compact('hosName', 'hosPhoneNo', 'hosEmail', 'hosDateRegis', 'hosPin', 'hosAddress', 'id'));
    }
}
