<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Booking;
use Illuminate\Support\Facades\Auth;
use Validator;
use Carbon\Carbon;
use App\UserDetails;

class BookingController extends Controller
{
    //
    public function createBooking(Request $request)
    {

        $validator =   Validator::make($request->all(), [
            'user_id' => 'required',
            'hospital_id' => 'required',
            'test_id' => 'required',
            'booking_date' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['Validation' => $validator->errors()]);
        }
        $booking = $request->all();
        $bookings = Booking::create($booking);
        return \response()->json($bookings->id);
    }

    public function authBookingList()
    {

        $listBooking = Booking::where('hospital_id', Auth::user()->id)->orderBy('booking_date', 'DESC')->get();
        return \response()->json($listBooking);
    }

    public function allBookingList()
    {

        $listBooking = Booking::orderBy('id', 'DESC')->get();
        return \response()->json($listBooking);
    }

    public function previousBooking()
    {

        $listBooking = Booking::where([['booking_date', '<', Carbon::today()],['id',Auth::user()->id]])
        ->with(['userList' => function ($e) {
            return $e->with('userDetails');
        }])
        ->with(['hospitalList' => function ($e) {
            return $e->with('hospitalDetails');
        }])
        ->orderBy('booking_date', 'DESC')
        ->get();
        // $listBooking = Booking::where([['booking_date','!=',Carbon::today()],['booking_date','!=',Carbon::tomorrow()]])->orderBy('booking_date','DESC')->get();
        return \response()->json($listBooking);
    }

    public function search(Request $request)
    {
        // $hi = $request->name;
        $searchList = Booking::
        whereHas('userList', function ($query) use($request){
            $query->where('name', 'like' , '%'.$request->name.'%')
            ->orWhere('email', 'like' , '%'.$request->name.'%')
            ->orWhereHas('userDetails', function ($q) use($request) {
             return   $q->where('age', 'like','%'.$request->name.'%')
             ->orWhere('gender', 'like','%'.$request->name.'%')
             ->orWhere('address', 'like','%'.$request->name.'%');
            });
        })
        ->orWhereHas('hospitalList', function ($query) use($request){
            $query->whereHas('hospitalDetails', function ($q) use($request) {
             return   $q->where('hospital_name','like','%'.$request->name.'%');
            });
        })
        // ->with(['userList' => function ($e) {
        //     return $e->select('id')
        //     ->with(['userDetails'=> function ($e) {
        //         return $e->select('id');
        //     }]);
        // }])
        ->with(['userList' => function ($e) {
            return $e->with('userDetails');
        }])
        ->with(['hospitalList' => function ($e) {
            return $e->with('hospitalDetails');
        }])
        ->get();

        return response()->json($searchList);
    }

    public function hospitalDashboard() {
        $booking = Booking::where('id',Auth::user()->id)->get();
        $bookingCount = $booking->count();
        $bookingTest = Booking::where('id',Auth::user()->id)
        ->where('is_report_generated',1)
        ->get();
        $bookingTestCompletedCount = $bookingTest->count();
        return response()->json($bookingCount,$bookingTestCompletedCount);


    }
    public function authTomorrow()
    {
        $hospital = Booking::where('id',Auth::user()->id)
        ->whereDate('booking_date', Carbon::tomorrow())
        ->get();
        return response()->json(["today" => $hospital]);
    }

    public function authToday()
    {
        $hospital1 = Booking::where('id',Auth::user()->id)->whereDate('booking_date', Carbon::today())->get();

        return response()->json(["tomorrow" => $hospital1]);
    }


    public function allTomorrow()
    {
        $hospital = Booking::
        whereDate('booking_date', Carbon::tomorrow())
        ->with(['userList' => function ($e) {
            return $e->with('userDetails');
        }])
        ->with(['hospitalList' => function ($e) {
            return $e->with('hospitalDetails');
        }])
        ->get();
        return response()->json(["today" => $hospital]);
    }

    public function allToday()
    {
        $hospital1 = Booking::whereDate('booking_date', Carbon::today())
        ->with(['userList' => function ($e) {
            return $e->with('userDetails');
        }])
        ->with(['hospitalList' => function ($e) {
            return $e->with('hospitalDetails');
        }])
        ->get();

        return response()->json(["tomorrow" => $hospital1]);
    }


}
