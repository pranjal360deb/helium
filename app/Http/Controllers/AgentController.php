<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Agent;
use App\User;
use App\UserDetails;
use League\Flysystem\Exception;
use App\Booking;
use Illuminate\Support\Str;
use Brian2694\Toastr\Facades\Toastr;


class AgentController extends Controller
{
    //
    // public function createAgent(Request $request) {
    //     $agent = Agent::create($input);
    //      $input = $request->all();
    //    return response()->json($agent);
    // }

    public function getAgent()
    {
        $agents = User::with('userDetails')
       ->where('role_id',4)
       ->orderBy('id', 'DESC')->paginate(10);

       return view('admin.agent.agent',compact('agents'));

        // return response()->json($agents);
    }
    public function search(Request $request) {

        $agents = User::where('role_id',4)
        ->where('is_deactivate','like' , '%'.$request->status.'%')
        ->where(function ($q) use ($request){
            $q->where('name','like' , '%'.$request->name.'%')
            ->orWhere('email','like' , '%'.$request->name.'%')
            ->orWhere('phone_no','like' , '%'.$request->name.'%')
            // ->orWhere('is_deactivate', 'like' , '%'.$request->status.'%')
            ->orWhereHas('userDetails', function ($query) use($request){
                $query->where('pin', 'like' , '%'.$request->name.'%')
                ->orWhere('address', 'like' , '%'.$request->name.'%');
                // ->orWhere('is_deactivate', 'like' , '%'.$request->name.'%');
            })
            ;
        })
        ->with('userDetails')
        ->orderBy('id', 'DESC')
        ->paginate(10);

        // dd($hospitalLists,$request->status);
        return view('admin.agent.agent',compact('agents'));

    }

    public function createAgent(Request $request)
    {


        $check = User::where('role_id', 4)
            ->where('email', $request->email)
            ->first();

        if ($check == null) {
            // DB::beginTransaction();

            try {
                $uniqid = random_int(10000,99999);

                $this->validate($request, [
                    'name' => 'required',
                    'email' => 'required|email|unique:users',
                    // 'phone_no' => 'required|regex:/[0-9][0-9]{9}/|numeric',
                    'phone' => 'required|numeric|digits_between:10,12',
                    // 'pin' => 'required|regex:/[0-9][0-9]{5}/|numeric',

                    // 'password' => 'required',
                    ],
                // [
                //     // 'pin.regex' => 'Minimum 6 digit required for zip code',
                //     // 'phone_no.regex' => 'Minimum 10 digit required for phone Number',
                //     // 'phone_no.max' => 'Phone Number can\'t be greater than 12 digit'

                // ]

            );

                $agents['role_id'] = 4;
                $agents['name'] = $request->name;
                $agents['phone_no'] = $request->phone;
                $agents['email'] = $request->email;
                $agents['unique_id'] = $uniqid;

                $user = User::create($agents);


                $agentsDetails['user_id'] = $user->id;
                $agentsDetails['address'] = $request->address;
                $agentsDetails['gender'] = $request->gender;
                $agentsDetails['pin'] = $request->pin;
                $agentsDetails['age'] = $request->age;

                $details = UserDetails::create($agentsDetails);
                Toastr::success('Agent Created successfully','Success');
                return \redirect()->back();
            } catch (Exception $e) {
                // DB::rollback();
                Log::error($e);
            }
            // DB::commit();
        } else {
            Toastr::warning('Agent Exist','Warning');
            return \redirect()->back();
            // return response()->json('exist Agent');
        }
    }

    public function editAgent($id){
        $hospitalLists = User::with('userDetails')
        ->where('id',$id)
        ->where('role_id', 4)
        ->orderBy('id', 'DESC')
        ->get();

        foreach ($hospitalLists as $item) {

            $hosName = $item->name;
            // $hosRegistration = $item->hospitalDetails->registration_number;
            $hosAddress = $item->userDetails->address;
            $hosEmail = $item->email;
            $hosPin = $item->userDetails->pin;
            $hosPhoneNo = $item->phone_no;
            $hosDateRegis = $item->created_at;
        }

        return  view('admin.agent.edit',compact('hosName','hosPhoneNo','hosEmail','hosDateRegis','hosPin','hosAddress','id' ));


    }


    public function viewAgent($id){
        $hospitalLists = User::with('userDetails')
        ->where('id',$id)
        ->where('role_id', 4)
        ->orderBy('id', 'DESC')
        ->get();

        foreach ($hospitalLists as $item) {

            $hosName = $item->name;
            $hosUnique = $item->unique_id;
            $hosAddress = $item->userDetails->address;
            $hosEmail = $item->email;
            $hosPin = $item->userDetails->pin;
            $hosPhoneNo = $item->phone_no;
            $hosDateRegis = $item->created_at;
        }

        // dd($hosName);
        // return  view('admin.agent.view',compact('hosName'));


        return  view('admin.agent.view',compact('hosName','hosPhoneNo','hosEmail','hosDateRegis','hosUnique','hosPin','hosAddress','id' ));


    }

    public function updateAgent(Request $request) {
        $id = $request->id;


        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            // 'phone_no' => 'required|regex:/[0-9][0-9]{9}/|numeric',
            'phone' => 'required|numeric|digits_between:10,12',
            // 'pin' => 'required|regex:/[0-9][0-9]{5}/|numeric',

            // 'password' => 'required',
            ],
        // [
        //     // 'pin.regex' => 'Minimum 6 digit required for zip code',
        //     // 'phone_no.regex' => 'Minimum 10 digit required for phone Number',
        //     // 'phone_no.max' => 'Phone Number can\'t be greater than 12 digit'

        // ]

        );

        $user =  User::find($id);
        $user->name = $request->name;
        $user->phone_no = $request->phone;
        $user->email = $request->email;
        $user->save();

        $hospital = UserDetails::where('user_id',$id)->first();
        // $hospital->pin =  $request->pin;
        $hospital->address =  $request->address;
        $hospital->save();
        // dd($id);
        Toastr::success('Agent Edited successfully','Success');
        return \redirect()->back();

    }


    public function deactivateAgent(Request $request) {
        $status = $request->value;
        $id = $request->id;
        $user =  User::find($id);
        $user->is_deactivate =  $status;
        $user->save();

        // dd($status);
        return response()->json($user->is_deactivate);
        // return \redirect()->back();


    }
    public function asignAgent(Request $request) {
        $bookingId  = $request->id;
        $otp= random_int(100000,999999);
        $booking = Booking::where('id',$bookingId)->first();
        $booking->otp = $otp;
        $booking->agent_id = $request->agent_id;
        $booking->save();
        return \redirect()->to('/booking');
        // return \response()->json(['success'=>1]);
    }


    public function deleteAgent($id)
    {
        $agent = User::where('id', $id)->where('role_id', 4)->first();
        $agent->delete();
        $agentDetails = UserDetails::where('user_id', $id);
        $agentDetails->delete();
        Toastr::success('Agent Deleted successfully','Success');

        return \redirect()->back();
    }

    public function dicativateAgent(Request $request, $id)
    {
        $user = User::where('id', $id)->where('role_id', 4)->first();
        $user->is_deactivate = $request->deactivate;
        $user->save();

        $userDetails = UserDetails::where('user_id', $id)->first();
        $userDetails->is_deactivate = $request->deactivate;
        $userDetails->save();

        return response()->json(['success' => 200]);
    }
}
