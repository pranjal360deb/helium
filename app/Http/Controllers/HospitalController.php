<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hospital;
use Carbon\Carbon;
use App\Booking;
use Illuminate\Support\Facades\Auth;
use App\User;

class HospitalController extends Controller
{
    //
    public function tomorrow()
    {
        $hospital = Hospital::where('id',Auth::user()->id)
        ->whereDate('created_at', Carbon::tomorrow())
        ->get();
        return response()->json(["today" => $hospital]);
    }

    public function today()
    {
        $hospital1 = Hospital::where('id',Auth::user()->id)->whereDate('created_at', Carbon::today())->get();

        return response()->json(["tomorrow" => $hospital1]);
    }

    public function dashboard()
    {
        $totalBooking = Booking::where('hospital_id', Auth::user()->id)->get();
        $bookingCount = $totalBooking->count();
        $testCompleted = Booking::where([['is_report_generated', 1], ['hospital_id', Auth::user()->id]])->get();

        $testCompletedCount = $testCompleted->count();
        $listBooking = Booking::where([['booking_date', '>=', Carbon::today()]])->orderBy('booking_date', 'DESC')->get();
        $listBookingCount = $listBooking->count();


        return \response()->json($bookingCount, $testCompletedCount, $listBookingCount);
    }

    public function authBooking()
    {
        $details = Booking::where('hospital_id', Auth::user()->id)
            ->with(['userList' => function ($e) {
                return $e->with('userDetails');
            }])
            ->with(['hospitalList' => function ($e) {
                return $e->with('hospitalDetails');
            }])
            ->get();
        return \response()->json($details);
    }

    public function allBooking()
    {
        $bookings = Booking::with(['userList' => function ($e) {
            return $e->with('userDetails');
        }])
        ->with(['agentList' => function ($e) {
            return $e->with('userDetails');
        }])
            ->with(['hospitalList' => function ($e) {
                return $e->with('hospitalDetails');
            }])
            ->paginate(10);
            $agents = User::where('role_id',4)->get();
            // dd($details);
            return view('admin.booking.booking', compact('bookings','agents') );
        // return \response()->json($details);
    }


    public function allPatient()
    {
        $bookings = Booking::with(['userList' => function ($e) {
            return $e->with('userDetails');
        }])
            ->with(['hospitalList' => function ($e) {
                return $e->with('hospitalDetails');
            }])
            ->paginate(10);
            // dd($details);
            return view('admin.patient.patient', compact('bookings') );
        // return \response()->json($details);
    }

    public function authPatient($id)
    {
        $bookings = Booking::where('id',$id)->with(['userList' => function ($e) {
            return $e->with('userDetails');
        }])
            ->with(['hospitalList' => function ($e) {
                return $e->with('hospitalDetails');
            }])
            ->first();
            // foreach ($)
            // dd($details);
            return view('admin.patient.profile', compact('bookings') );
        // return \response()->json($details);
    }

    public function viewBooking($id) {

        $bookings = Booking::with(['userList' => function ($e) {
            return $e->with('userDetails');
        }])
            ->with(['hospitalList' => function ($e) {
                return $e->with('hospitalDetails');
            }])
            ->with(['agentList' => function ($e) {
                return $e->with('userDetails');
            }])
            ->where('user_id',$id)
            ->first();

        // foreach ($bookings as $item) {

        //     $hosName = $item->userList->name;
        //     $hosBooking = $item->booking_date;
        //     $hosAddress = $item->hospitalList->hospitalDetails->address;
        //     $hosEmail = $item->userList->email;
        //     $hospitalName = $item->hospitalList->hospitalDetails->hospital_name;
        //     $hospitalPin = $item->hospitalList->hospitalDetails->pin;
        //     $hosPhoneNo = $item->userList->phone_no;
        //     // $hosDateRegis = $item->created_at;
        // }

        // return  view('admin.booking.view',compact('hosName','id','hosBooking','hosPhoneNo', 'hosEmail', 'hospitalName' , 'hospitalPin', 'hosAddress' ));

        return  view('admin.booking.view',compact('bookings'));


    }

    public function viewHistory($id) {

        $bookings = Booking::with(['userList' => function ($e) {
            return $e->with('userDetails');
        }])
            ->with(['hospitalList' => function ($e) {
                return $e->with('hospitalDetails');
            }])
            ->where('user_id',$id)
            ->get();
            return  view('admin.booking.history',compact('bookings' ));
        }


}
