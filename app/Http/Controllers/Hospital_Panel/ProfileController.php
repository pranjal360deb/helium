<?php

namespace App\Http\Controllers\Hospital_Panel;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;

class ProfileController extends Controller
{
    //
    public function hospitalProfile(){
        $profile = User::where('id', Auth::user()->id)->with('hospitalDetails')->first();
        return view('hospital.profile.profile',compact('profile'));
    }
}
