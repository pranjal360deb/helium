<?php

namespace App\Http\Controllers\Hospital_Panel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Booking;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class DashboardController extends Controller
{
    //
    public function dashboard()
    {
        // dd("hello");
        $booking =  Booking::where('hospital_id', Auth::user()->id)->get();
        $bookingCount = $booking->count();


        $bookingSaturday = Booking::where('hospital_id', Auth::user()->id)->get();
        $filteredArray = array();
        $fridayArray = array();
        $thursdayArray = array();
        $wednesdayArray = array();
        $tuesdayArray = array();
        $mondayArray = array();

        $firstYearArray = array();
        $thirdYearArray = array();
        $secYearArray = array();
        $fourthYearArray = array();
        $fifthYearArray = array();
        $sixthYearArray = array();

        //yearly
        $januaryArray = array();
        $febArray = array();
        $marArray = array();
        $aprArray = array();
        $mayArray = array();
        $juneArray = array();
        $julyArray = array();
        $augustArray = array();
        $septArray = array();
        $octoberArray = array();
        $novArray = array();
        $decArray = array();



        foreach ($bookingSaturday as $record) {
            //weekly
            if (Carbon::parse($record->created_at)->dayOfWeek == 6) {
                $filteredArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->dayOfWeek == 5) {
                $fridayArray[] = $record;
            }

            if (Carbon::parse($record->created_at)->dayOfWeek == 4) {
                $thursdayArray[] = $record;
            }

            if (Carbon::parse($record->created_at)->dayOfWeek == 3) {
                $wednesdayArray[] = $record;
            }

            if (Carbon::parse($record->created_at)->dayOfWeek == 2) {
                $tuesdayArray[] = $record;
            }

            if (Carbon::parse($record->created_at)->dayOfWeek == 1) {
                $mondayArray[] = $record;
            }

            //yearly
            if (Carbon::parse($record->created_at)->year == 2020) {
                $firstYearArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->year == 2021) {
                $secYearArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->year == 2022) {
                $thirdYearArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->year == 2023) {
                $fourthYearArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->year == 2024) {
                $fifthYearArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->year == 2025) {
                $sixthYearArray[] = $record;
            }

            //monthly
            if (Carbon::parse($record->created_at)->month == 01) {
                $januaryArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 02) {
                $febArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 03) {
                $marArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 04) {
                $aprArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 05) {
                $mayArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 06) {
                $juneArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 07) {
                $julyArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == '08') {
                $augustArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == '09') {
                $septArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 10) {
                $octoberArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 11) {
                $novArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 12) {
                $decArray[] = $record;
            }
        }
        //weekly
        $bookingSaturdayCount = sizeof($filteredArray);
        $bookingFridayCount = sizeof($fridayArray);
        $bookingThrusdayCount = sizeof($thursdayArray);
        $bookingWednesdayCount = sizeof($wednesdayArray);
        $bookingTuesdayCount = sizeof($tuesdayArray);
        $bookingMondayCount = sizeof($mondayArray);

        //yearly
        $firstYearArrayCount = sizeof($firstYearArray);
        $secYearArrayCount = sizeof($secYearArray);
        $thirdYearArrayCount = sizeof($thirdYearArray);
        $fourthYearArrayCount = sizeof($fourthYearArray);
        $fifthYearArrayCount = sizeof($fifthYearArray);
        $sixthYearArrayCount = sizeof($sixthYearArray);

        //monthly
        $januaryArrayCount = sizeof($januaryArray);
        $febArrayCount = sizeof($febArray);
        $marArrayCount = sizeof($marArray);
        $aprArrayCount = sizeof($aprArray);
        $mayArrayCount = sizeof($mayArray);
        $juneArrayCount = sizeof($juneArray);
        $julyArrayCount = sizeof($julyArray);
        $augustArrayCount = sizeof($augustArray);
        $septArrayCount = sizeof($septArray);
        $octoberArrayCount = sizeof($octoberArray);
        $novArrayCount = sizeof($novArray);
        $decArrayCount = sizeof($decArray);



        $bookingSunday = Booking::where('hospital_id', Auth::user()->id)->get();
        $sundayArray = array();
        foreach ($bookingSunday as $record) {
            if (Carbon::parse($record->created_at)->dayOfWeek == 0) {
                $sundayArray[] = $record;
            }
        }
        $bookingSundayCount = sizeof($sundayArray);

        $authBook = Booking::where('hospital_id', Auth::user()->id)->with(['userList' => function ($e) {
            return $e->with('userDetails');
        }])
            ->with(['hospitalList' => function ($e) {
                return $e->with('hospitalDetails');
            }])
            ->paginate(10);

        return view('hospital.dashboard.dashboard', compact(
            'bookingCount',
            'bookingSaturdayCount',
            'bookingSundayCount',
            'bookingFridayCount',
            'bookingThrusdayCount',
            'bookingMondayCount',
            'bookingTuesdayCount',
            'bookingWednesdayCount',
            'firstYearArrayCount',
            'secYearArrayCount',
            'thirdYearArrayCount',
            'fourthYearArrayCount',
            'fifthYearArrayCount',
            'sixthYearArrayCount',

            'octoberArrayCount',
            'januaryArrayCount',
            'febArrayCount',
            'marArrayCount',
            'aprArrayCount',
            'mayArrayCount',
            'juneArrayCount',
            'julyArrayCount',
            'augustArrayCount',
            'septArrayCount',
            'decArrayCount',
            'novArrayCount',

            'authBook'

        ));
    }


    public function search(Request $request)
    {
        // dd("hello");
        $booking =  Booking::where('hospital_id', Auth::user()->id)->get();
        $bookingCount = $booking->count();


        $bookingSaturday = Booking::where('hospital_id', Auth::user()->id)->get();
        $filteredArray = array();
        $fridayArray = array();
        $thursdayArray = array();
        $wednesdayArray = array();
        $tuesdayArray = array();
        $mondayArray = array();

        $firstYearArray = array();
        $thirdYearArray = array();
        $secYearArray = array();
        $fourthYearArray = array();
        $fifthYearArray = array();
        $sixthYearArray = array();

        //yearly
        $januaryArray = array();
        $febArray = array();
        $marArray = array();
        $aprArray = array();
        $mayArray = array();
        $juneArray = array();
        $julyArray = array();
        $augustArray = array();
        $septArray = array();
        $octoberArray = array();
        $novArray = array();
        $decArray = array();



        foreach ($bookingSaturday as $record) {
            //weekly
            if (Carbon::parse($record->created_at)->dayOfWeek == 6) {
                $filteredArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->dayOfWeek == 5) {
                $fridayArray[] = $record;
            }

            if (Carbon::parse($record->created_at)->dayOfWeek == 4) {
                $thursdayArray[] = $record;
            }

            if (Carbon::parse($record->created_at)->dayOfWeek == 3) {
                $wednesdayArray[] = $record;
            }

            if (Carbon::parse($record->created_at)->dayOfWeek == 2) {
                $tuesdayArray[] = $record;
            }

            if (Carbon::parse($record->created_at)->dayOfWeek == 1) {
                $mondayArray[] = $record;
            }

            //yearly
            if (Carbon::parse($record->created_at)->year == 2020) {
                $firstYearArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->year == 2021) {
                $secYearArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->year == 2022) {
                $thirdYearArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->year == 2023) {
                $fourthYearArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->year == 2024) {
                $fifthYearArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->year == 2025) {
                $sixthYearArray[] = $record;
            }

            //monthly
            if (Carbon::parse($record->created_at)->month == 01) {
                $januaryArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 02) {
                $febArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 03) {
                $marArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 04) {
                $aprArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 05) {
                $mayArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 06) {
                $juneArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 07) {
                $julyArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == '08') {
                $augustArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == '09') {
                $septArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 10) {
                $octoberArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 11) {
                $novArray[] = $record;
            }
            if (Carbon::parse($record->created_at)->month == 12) {
                $decArray[] = $record;
            }
        }
        //weekly
        $bookingSaturdayCount = sizeof($filteredArray);
        $bookingFridayCount = sizeof($fridayArray);
        $bookingThrusdayCount = sizeof($thursdayArray);
        $bookingWednesdayCount = sizeof($wednesdayArray);
        $bookingTuesdayCount = sizeof($tuesdayArray);
        $bookingMondayCount = sizeof($mondayArray);

        //yearly
        $firstYearArrayCount = sizeof($firstYearArray);
        $secYearArrayCount = sizeof($secYearArray);
        $thirdYearArrayCount = sizeof($thirdYearArray);
        $fourthYearArrayCount = sizeof($fourthYearArray);
        $fifthYearArrayCount = sizeof($fifthYearArray);
        $sixthYearArrayCount = sizeof($sixthYearArray);

        //monthly
        $januaryArrayCount = sizeof($januaryArray);
        $febArrayCount = sizeof($febArray);
        $marArrayCount = sizeof($marArray);
        $aprArrayCount = sizeof($aprArray);
        $mayArrayCount = sizeof($mayArray);
        $juneArrayCount = sizeof($juneArray);
        $julyArrayCount = sizeof($julyArray);
        $augustArrayCount = sizeof($augustArray);
        $septArrayCount = sizeof($septArray);
        $octoberArrayCount = sizeof($octoberArray);
        $novArrayCount = sizeof($novArray);
        $decArrayCount = sizeof($decArray);



        $bookingSunday = Booking::where('hospital_id', Auth::user()->id)->get();
        $sundayArray = array();
        foreach ($bookingSunday as $record) {
            if (Carbon::parse($record->created_at)->dayOfWeek == 0) {
                $sundayArray[] = $record;
            }
        }
        $bookingSundayCount = sizeof($sundayArray);

        if($request->day == 1)

       {
        $authBook = Booking::whereDate('booking_date', Carbon::today())->where('hospital_id', Auth::user()->id)->with(['userList' => function ($e) {
            return $e->with('userDetails');
        }])
            ->with(['hospitalList' => function ($e) {
                return $e->with('hospitalDetails');
            }])
            ->paginate(10);
        }

        if($request->day == 2)

        {
         $authBook = Booking::whereDate('booking_date', Carbon::tomorrow())->where('hospital_id', Auth::user()->id)->with(['userList' => function ($e) {
             return $e->with('userDetails');
         }])
             ->with(['hospitalList' => function ($e) {
                 return $e->with('hospitalDetails');
             }])
             ->paginate(10);
         }

         if($request->day == 3)

         {
          $authBook = Booking::where('hospital_id', Auth::user()->id)->with(['userList' => function ($e) {
              return $e->with('userDetails');
          }])
              ->with(['hospitalList' => function ($e) {
                  return $e->with('hospitalDetails');
              }])
              ->paginate(10);
          }

        return view('hospital.dashboard.dashboard', compact(
            'bookingCount',
            'bookingSaturdayCount',
            'bookingSundayCount',
            'bookingFridayCount',
            'bookingThrusdayCount',
            'bookingMondayCount',
            'bookingTuesdayCount',
            'bookingWednesdayCount',
            'firstYearArrayCount',
            'secYearArrayCount',
            'thirdYearArrayCount',
            'fourthYearArrayCount',
            'fifthYearArrayCount',
            'sixthYearArrayCount',

            'octoberArrayCount',
            'januaryArrayCount',
            'febArrayCount',
            'marArrayCount',
            'aprArrayCount',
            'mayArrayCount',
            'juneArrayCount',
            'julyArrayCount',
            'augustArrayCount',
            'septArrayCount',
            'decArrayCount',
            'novArrayCount',

            'authBook'

        ));
    }
}
