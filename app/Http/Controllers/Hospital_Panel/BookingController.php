<?php

namespace App\Http\Controllers\Hospital_Panel;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Booking;
use Illuminate\Support\Facades\Auth;

class BookingController extends Controller
{
    //
    public function authBooking()
    {
        $bookings = Booking::where('hospital_id', Auth::user()->id)->with(['userList' => function ($e) {
            return $e->with('userDetails');
        }])
            ->with(['agentList' => function ($e) {
                return $e->with('userDetails');
            }])
            ->with(['hospitalList' => function ($e) {
                return $e->with('hospitalDetails');
            }])
            ->paginate(10);
        // $agents = User::where('role_id',4)->get();
        // dd($bookings);
        return view('hospital.booking.booking', compact('bookings'));
        // return \response()->json($details);
    }

    public function verifyOTP(Request $request)
    {
        $id = $request->id;
        $bookings = Booking::where([['id', $id], ['hospital_id', Auth::user()->id]])->first();
        $otp = $request->otp;
        if ($bookings->otp == $otp) {
            $bookings->is_deposited = 1;
            $bookings->save();
            // dd($bookings->otp);
            return \redirect()->back();
        } else {
            dd('check again');
        }
    }

    public function viewBooking($id)
    {

        $bookings = Booking::where([['user_id', $id], ['hospital_id', Auth::user()->id]])
            ->with(['userList' => function ($e) {
                return $e->with('userDetails');
            }])
            ->with(['agentList' => function ($e) {
                return $e->with('userDetails');
            }])
            ->with(['hospitalList' => function ($e) {
                return $e->with('hospitalDetails');
            }])
            // ->where('user_id', $id)
            ->first();
        // dd($bookings);
        return view('hospital.booking.bookingDetails', compact('bookings'));
    }

    public function history($id) {

        $bookings = Booking::with(['userList' => function ($e) {
            return $e->with('userDetails');
        }])
        ->with(['agentList' => function ($e) {
            return $e->with('userDetails');
        }])
         ->with(['hospitalList' => function ($e) {
                return $e->with('hospitalDetails');
            }])
            ->where([['user_id',$id],['hospital_id', Auth::user()->id]])
            ->get();
        // dd($id);
        return view('hospital.booking.bookingHistory',compact('bookings'));
    }
    public function authPatient($id)
    {
        $bookings = Booking::where([['id',$id],['hospital_id', Auth::user()->id]])->with(['userList' => function ($e) {
            return $e->with('userDetails');
        }])
        ->with(['agentList' => function ($e) {
            return $e->with('userDetails');
        }])
            ->with(['hospitalList' => function ($e) {
                return $e->with('hospitalDetails');
            }])
            ->first();
            // foreach ($)
            // dd($details);
            return view('hospital.booking.patient', compact('bookings') );
        // return \response()->json($details);
    }
}
