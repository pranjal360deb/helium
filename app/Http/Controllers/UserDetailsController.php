<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserDetails;
use Illuminate\Support\Facades\Auth;
use function Opis\Closure\serialize;
use function Opis\Closure\unserialize;
use Illuminate\Support\Facades\DB;

class UserDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $usersDetails= DB::table('usersDetails')
        ->where('id',12)
        ->get()
        ->map(function($i) {
            $i->address = unserialize($i->address);
            return $i;
        });
    //    $data= unserialize($usersDetails->address);
        return response()->json($usersDetails);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // foreach ($request->get('address') as $answer) {
        //     $userDetails = UserDetails::create([
        //         'user_id' => Auth::id(),
        //         'address' => $answer,
        //     ]);
        // }

        // $input = $request->all();
        // $arr = array('x','y','z');
        // $se = serialize($arr);
        // $input['address']=$se;
        // $product = UserDetails::create($input);


        $input = $request->all();
        $document = $request->get('address');

        $images = array();
            foreach ($document as $file) {
                $images[] = $file;
            }
            // $input['photos']=$images;
        $input['address']=serialize($images);
        $product = UserDetails::create($input);

        // return response()->json($product);

        return response()->json($product);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
