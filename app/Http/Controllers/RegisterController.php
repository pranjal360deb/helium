<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\UserDetails;
use Mail;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;
use Config;
use Illuminate\Support\Facades\Hash;
use App\Roll;
use League\Flysystem\Exception;
use App\Hospital;
use Illuminate\Support\Str;
use Brian2694\Toastr\Facades\Toastr;

class RegisterController extends Controller
{
    public function createRoll(Request $request)
    {
        $input['roll_name'] = $request->roll_name;
        $roll = Roll::create($input);
        return response()->json(['success' => 1]);
    }
    public function usersList()
    {
        $usersList = User::with('userDetails')
            ->where('role_id', 1)
            ->orderBy('id', 'DESC')
            ->get();
        return response()->json($usersList);
    }

    public function hospitalList()
    {
        $hospitalLists = User::with('hospitalDetails')
            ->where('role_id', 2)
            ->orderBy('id', 'DESC')
            ->paginate(10);
            // dd($hospitalLists);
            return view('admin.hospital.hospital',compact('hospitalLists'));

        // return response()->json($usersList);
    }

    public function viewHospital($id){
        $hospitalLists = User::with('hospitalDetails')
        ->where('id',$id)
        ->where('role_id', 2)
        ->orderBy('id', 'DESC')
        ->get();

        foreach ($hospitalLists as $item) {

            $hosName = $item->hospitalDetails->hospital_name;
            $hosRegistration = $item->hospitalDetails->registration_number;
            $hosAddress = $item->hospitalDetails->address;
            $hosEmail = $item->email;
            $hosPin = $item->hospitalDetails->pin;
            $hosPhoneNo = $item->phone_no;
            $hosDateRegis = $item->created_at;




        }
        // dd($hosid);

        return  view('admin.hospital.view',compact('hosName','hosPhoneNo','hosEmail','hosRegistration','hosDateRegis','hosPin','hosAddress','id' ));

    }


    public function editHospital($id) {

        $hospitalLists = User::with('hospitalDetails')
        ->where('id',$id)
        ->where('role_id', 2)
        ->orderBy('id', 'DESC')
        ->get();

        foreach ($hospitalLists as $item) {

            $hosName = $item->hospitalDetails->hospital_name;
            $hosRegistration = $item->hospitalDetails->registration_number;
            $hosAddress = $item->hospitalDetails->address;
            $hosEmail = $item->email;
            $hosPin = $item->hospitalDetails->pin;
            $hosPhoneNo = $item->phone_no;
            $hosDateRegis = $item->created_at;
        }

        return  view('admin.hospital.edit',compact('hosName','hosPhoneNo','hosEmail','hosRegistration','hosDateRegis','hosPin','hosAddress','id' ));


    }

    public function editingHospital(Request $request){
        $id = $request->id;

        // dd($request->registration_no);
        $user =  User::find($id);
        $user->name = $request->hospital_name;
        $user->phone_no = $request->phone_no;
        // $user->email = $request->email;
        $user->save();

        $hospital = Hospital::where('user_id',$id)->first();
        $hospital->hospital_name =  $request->hospital_name;
        $hospital->pin =  $request->pin;
        $hospital->registration_number =  $request->registration_no;
        $hospital->address =  $request->address;
        $hospital->save();
        Toastr::success('Hospital Edited successfully','Success');
        return \redirect()->back();

    }

    public function deactivateHospital(Request $request) {
        $status = $request->value;
        $id = $request->id;
        $user =  User::find($id);
        $user->is_deactivate =  $status;
        $user->save();

        // dd($status);
        return response()->json($user->is_deactivate);
        // return \redirect()->back();


    }
     public function deleteHospital($id){

        $user = User::find($id);
        $user->delete();
        $hospital = Hospital::where('user_id',$id)->first();
        $hospital->delete();

        Toastr::success('Hospital Deleted successfully','Success');
        return redirect()->back();
     }

    public function agentList()
    {
        $usersList = User::with('userDetails')
            ->where('role_id', 4)
            ->orderBy('id', 'DESC')
            ->get();
        return response()->json($usersList);
    }
    public function register(Request $request)
    {

        switch ($request->role_id) {

            case '1':
                $check = User::where('role_id', $request->role_id)
                    ->where('email', $request->email)
                    ->first();
                if ($check == null) {
                    // DB::beginTransaction();
                    try {
                        $validator =   Validator::make($request->all(), [
                            'name' => 'required',
                            'email' => 'required',
                            'password' => 'required',
                        ]);

                        if ($validator->fails()) {
                            return response()->json(['Validation' => $validator->errors()], 401);
                        }
                        // $input = $request->all();
                        $input['name'] = $request->name;
                        $input['email'] = $request->email;
                        $input['phone_no'] = $request->phone_no;
                        $input['password'] = Hash::make($request->password);
                        $input['role_id'] = $request->role_id;
                        $user = User::create($input);
                        $accesstoken = $user->createToken('MyApp');
                        // $success['token'] =  $user->createToken('MyApp');
                        $success['token'] =  $accesstoken->accessToken;
                        $success['Expiery_time'] =  $accesstoken->token->expires_at->diffForHumans();
                        $success['name'] =  $user->name;

                        $userDetails['user_id'] = $user->id;
                        $userDetails['age'] = $request->age;
                        $userDetails['gender'] = $request->gender;
                        $userDetails['address'] = $request->address;
                        $userDetails['pin'] = $request->pin;
                        $userDetails['country_id'] = $request->country_id;
                        $userDetails['state_id'] = $request->state_id;
                        $userDetails['city_id'] = $request->city_id;

                        $details = UserDetails::create($userDetails);


                        return response()->json(['success' => 1]);
                    } catch (Exception $e) {
                        // DB::rollback();
                        Log::error($e);
                    }
                    // DB::commit();
                } else {
                    return response()->json(' exist');
                }
                break;
            case '3':
                return response()->json('3');
                break;
        }
    }

    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        if (Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password'), 'role_id' => 1])) {
            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            $success['name'] =  $user->name;

            return response()->json($success);
        } else {
            return response()->json(null, 401);
        }
    }

    public function loginHospital(Request $request)
    {
        if (Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password'), 'role_id' => 2])) {

            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            $success['name'] =  $user->name;
            // $success['expiry'] =  $user->refresh();
            // $http = new \GuzzleHttp\Client();

            // $response = $http->post('http://127.0.0.1:8000/oauth/token', [
            //     'form_params' => [
            //         'grant_type' => 'refresh_token',
            //         'refresh_token' => 'the-refresh-token',
            //         'client_id' => 'client-id',
            //         'client_secret' => 'client-secret',
            //         'scope' => '',
            //     ],
            // ]);

            // $success['refresh'] = A

            return response()->json($success);
        } else {
            return response()->json(null, 401);
        }
    }

    public function loginClient(Request $request)
    {
        if (Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password'), 'role_id' => 3])) {
            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            $success['name'] =  $user->name;

            return response()->json($success);
        } else {
            return response()->json(null, 401);
        }
    }

    public function checkPassword(Request $request)
    {

        // $pp = Hash::make($request->new_password);
        // return response()->json($pp);
        if (Hash::check($request->old_password, Auth::user()->password)) {

            $this->validate($request(), [
                'new_password' => 'required'

            ]);
            $id = Auth::user()->id;
            $user = User::find($id);
            $user->password =  Hash::make($request->new_password);
            $user->save();
            // return response()->json(['success' => 200]);
            return redirect()->back();
        } else {

            dd('not amtached');

            // return response()->json(['error' => ['The old password does not match our records.']]);
        }
        //

    }


    public function dicativateUser(Request $request, $id)
    {
        $user = User::find($id);
        $user->is_deactivate = $request->deactivate;
        $user->save();

        $userDetails = UserDetails::where('user_id', $id)->first();
        $userDetails->is_deactivate = $request->deactivate;
        $userDetails->save();

        return response()->json(['success' => 200]);
    }


    public function createHospital(Request $request)
    {

        $check = User::where('role_id', 2)
            ->where('email', $request->email)
            ->first();
        if ($check == null) {
            // DB::beginTransaction();
            $test = Str::random(20);
            $email = $request->email;

            // try {
                $this->validate($request, [
                'hospital_name' => 'required',
                'email' => 'required|email|unique:users',
                // 'phone_no' => 'required|regex:/[0-9][0-9]{9}/|numeric',
                'phone_no' => 'required|numeric|digits_between:10,12',
                'pin' => 'required|regex:/[0-9][0-9]{5}/|numeric',

                // 'password' => 'required',
                ],
            [
                'pin.regex' => 'Minimum 6 digit required for zip code',
                // 'phone_no.regex' => 'Minimum 10 digit required for phone Number',
                // 'phone_no.max' => 'Phone Number can\'t be greater than 12 digit'

            ]

        );

            // if ($validator->fails()) {
            //     return response()->json(['Validation' => $validator->errors()]);
            // }
            // $input = $request->all();
            $input['name'] = $request->hospital_name;
            $input['email'] = $request->email;
            $input['phone_no'] = $request->phone_no;
            $input['password'] = Hash::make($test);
            $input['role_id'] = 2;
            $user = User::create($input);
            $accesstoken = $user->createToken('MyApp');
            // $success['token'] =  $user->createToken('MyApp');
            $success['token'] =  $accesstoken->accessToken;
            $success['Expiery_time'] =  $accesstoken->token->expires_at->diffForHumans();
            $success['name'] =  $user->name;

            $hospitalDetails['user_id'] = $user->id;
            $hospitalDetails['hospital_name'] = $request->hospital_name;
            $hospitalDetails['address'] = $request->address;
            $hospitalDetails['pin'] = $request->pin;
            $hospitalDetails['registration_number'] = $request->registration_number;
            $hospitalDetails['country_id'] = $request->country_id;
            $hospitalDetails['state_id'] = $request->state_id;
            $hospitalDetails['city_id'] = $request->city_id;
            $details = Hospital::create($hospitalDetails);




            $details = [
                'title' => 'Hello ' . $request->hospital_name . ' Hospital this is your Password !!',
                'body' => $test,
                'warning' => 'please change your password after login'
            ];

            \Mail::to($email)->send(new \App\Mail\MyTestMail($details));
            // echo "Basic Email Sent. Check your inbox.";
            // return response()->json(['success' => 200]);

            Toastr::success('Hospital Created successfully','Success');
            return \redirect()->back();
            // return response()->json($success);
            // } catch (Exception $e) {
            //     // DB::rollback();
            //     Log::error($e);
            // }
            // DB::commit();
        } else {
            Toastr::warning('exist hospital','Warning');
            // return response()->json('exist hospital');
            return \redirect()->back();

        }
    }


    public function dicativateHospital(Request $request, $id)
    {
        $user = User::find($id);
        $user->is_deactivate = $request->deactivate;
        $user->save();

        $userDetails = Hospital::where('user_id', $id)->first();
        $userDetails->is_deactivate = $request->deactivate;
        $userDetails->save();

        return response()->json(['success' => 200]);
    }


    public function search(Request $request) {

        $hospitalLists = User::where('role_id',2)
        ->where('is_deactivate','like' , '%'.$request->status.'%')
        ->where(function ($q) use ($request){
            $q->where('name','like' , '%'.$request->name.'%')
            ->orWhere('email','like' , '%'.$request->name.'%')
            ->orWhere('phone_no','like' , '%'.$request->name.'%')
            // ->orWhere('is_deactivate', 'like' , '%'.$request->status.'%')
            ->whereHas('hospitalDetails', function ($query) use($request){
                $query->where('hospital_name', 'like' , '%'.$request->name.'%')
                ->orWhere('pin', 'like' , '%'.$request->name.'%')
                ->orWhere('address', 'like' , '%'.$request->name.'%');
                // ->orWhere('is_deactivate', 'like' , '%'.$request->name.'%');
            });
        })
        ->with('hospitalDetails')
        ->orderBy('id', 'DESC')
        ->paginate(10);

        // dd($hospitalLists,$request->status);
        return view('admin.hospital.hospital',compact('hospitalLists'));

    }
}
