<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Roll extends Model
{
    //
    use SoftDeletes;

    protected $table = 'roles';
    // protected $fillable = ['name', 'detail'];
    protected $guarded = [];
    protected $dates = ['deleted_at'];
}
