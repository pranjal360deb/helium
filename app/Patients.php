<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Patients extends Model
{
    //
    use SoftDeletes;

    protected $table = 'patients';
    // protected $fillable = ['name', 'detail'];
    protected $guarded = [];
    protected $dates = ['deleted_at'];
}
