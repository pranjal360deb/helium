<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    //
    use SoftDeletes;

    protected $table = 'booking';
    protected $guarded = [];
    protected $dates = ['deleted_at'];

    public function userList() {
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function agentList() {
        return $this->belongsTo(User::class,'agent_id','id');
    }

    public function hospitalList() {
        return $this->belongsTo(User::class,'hospital_id','id');
    }
}
