<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::post('register', 'RegisterController@register');
Route::post('login', 'RegisterController@login');
Route::post('loginHospital', 'RegisterController@loginHospital');

Route::post('createRoll','RegisterController@createRoll');

Route::post('createBooking', 'BookingController@createBooking');



// Route::prefix('userDetails')->group(function () {
//     Route::post('insert', 'UserDetailsController@store');
//     Route::get('getDetails', 'UserDetailsController@index');

// });
// Route::post('products', 'ProductController@store');
// Route::resource('products', 'ProductController')->name('products');
// Route::post('products', 'ProductController@store');
Route::middleware('auth:api')->group(function () {


    // Route::get('authBookingList', 'BookingController@authBookingList');
    // Route::get('allbooking', 'BookingController@allBookingList');
    Route::get('authBooking', 'HospitalController@authBooking');









    Route::prefix('admin')->group(function () {
         //agent
         Route::post('createAgent', 'AgentController@createAgent');
         Route::get('getAgent', 'AgentController@getAgent');
         Route::post('asignAgent', 'AgentController@asignAgent');
         Route::post('deleteAgent/{id}', 'AgentController@deleteAgent');
         Route::post('dicativateAgent/{id}', 'AgentController@dicativateAgent');

         //hospital
         Route::post('createHospital', 'RegisterController@createHospital');
         Route::post('hospital/{id}','RegisterController@dicativateHospital');
         Route::get('hospitalList','RegisterController@hospitalList');

         //user
         Route::get('usersList','RegisterController@usersList');

         //dashboard
         Route::get('dashboard','AdminController@dashboard');

         //booking
         Route::post('search', 'BookingController@search');
         Route::get('allBooking', 'HospitalController@allBooking');
        //  Route::get('tomorrow', 'HospitalController@tomorrow');
         Route::post('today', 'BookingController@allToday');
         Route::post('tomorrow', 'BookingController@allTomorrow');
    });

    Route::prefix('hospital')->group(function () {

        //change password
        Route::post('checkpassword', 'RegisterController@checkPassword');

        //booking
        Route::get('authBooking', 'HospitalController@authBooking');


        //dashboard
        Route::get('hospitalDashboard', 'BookingController@hospitalDashboard');


        //history
        Route::get('previousBooking', 'BookingController@previousBooking');

    });





    // Route::prefix('userDetails')->group(function () {
    //     Route::post('insert', 'UserDetailsController@store');
    // });



});

// Route::get('allProductsPhotos', 'ProductController@index')->middleware('admin');

// Route::group(['middleware' => ['auth:api'] ], function(){
//         Route::post('products', 'ProductController@store');
// });
