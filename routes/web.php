<?php

use Illuminate\Support\Facades\Route;
use App\Events\UserNotification;
use Carbon\Carbon;
use App\Notifications\UserRegistration;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/test', function () {
//     return view('auth1.login');
//     // return response()->json(null, 401);
// })->name('login');

// Route::get('notification', function () {
//     return view('admin.common.notification');
// })->name('notification');

// Route::get('sender', function () {
//     return view('admin.common.sender');
//     // $text = request()->text;
//     // event(new UserNotification($text));
// })->name('sender');

Route::post('senderTest', function () {
    // return view('admin.common.sender');
    $text = request()->text;
    event(new UserNotification($text));
})->name('senderTest');

Route::get('newNotify', function () {
    $when = Carbon::now()->addSeconds(10);
    $user = App\User::find(1);
    $text =  $user;

    // User::find(1)->notify((new UserRegistration)->delay($when));
    App\User::find(12)->notify(new UserRegistration($text));
})->name('newNotify');

Route::get('/', function () {
    return view('auth.login');
    // return response()->json(null, 401);
});

// Route::post('login', 'RegisterController@login')->name('login');
Route::middleware('auth')->group(function () {


    Route::get('sender', function () {
        // return view('admin.common.sender');
        dd(auth()->user()->notifications);
        // $text = request()->text;
        // event(new UserNotification($text));
    })->name('sender');
//notificatin
Route::get('notification', function () {
        return view('admin.common.notification1');
})->name('notification');

//hospital
Route::get('/hospital','RegisterController@hospitalList')->name('hospital');
Route::get('hospital/create-hospital', function () {
    return view ('admin.hospital.create');
})->name('create.hospital');
Route::get('hospital/edit-hospital/{id}', 'RegisterController@editHospital')->name('edit.hospital');
Route::get('hospital/view-hospital/{id}', 'RegisterController@viewHospital')->name('view.hospital');
Route::post('creating', 'RegisterController@createHospital' )->name('creating.hospital');
Route::post('editing', 'RegisterController@editingHospital' )->name('editing.hospital');
Route::post('active', 'RegisterController@deactivateHospital' )->name('active');
Route::post('deleteHospital/{id}', 'RegisterController@deleteHospital')->name('deleteHospital');
Route::post('hospital/search','RegisterController@search')->name('hospital.search');

//agent
Route::get('agent', 'AgentController@getAgent' )->name('agent');
Route::get('agent/create-agent', function () {
    return view ('admin.agent.create');
})->name('create.agent');
Route::post('creatingAgent', 'AgentController@createAgent')->name('creating.agent');
Route::get('agent/view-agent/{id}', 'AgentController@viewAgent')->name('view.agent');

Route::get('agent/edit-agent/{id}', 'AgentController@editAgent')->name('edit.agent');
Route::post('editingAgent', 'AgentController@updateAgent' )->name('editing.agent');
Route::post('activeAgent', 'AgentController@deactivateAgent' )->name('activeAgent');
Route::post('deleteAgent/{id}', 'AgentController@deleteAgent')->name('deleteAgent');
Route::post('agent/search','AgentController@search')->name('agent.search');


//users
Route::get('users', 'AdminController@getUsers')->name('users');
Route::get('users/view/{id}','AdminController@viewUsers')->name('users.view');
Route::post('users/search','AdminController@search')->name('user.search');

// Route::post('active', 'RegisterController@deactivateHospital' )->name('active');

Route::get('settings', function () {
    return view('admin.settings.settings');
})->name('settings');

//CheckPassword
Route::post('checkpassword', 'RegisterController@checkPassword')->name('checkpassword');

Route::get('booking', 'HospitalController@allBooking')->name('booking');
Route::get('booking/details/{id}', 'HospitalController@viewBooking')->name('booking.details');

Route::get('booking/history/{id}','HospitalController@viewHistory')->name('booking.history');
Route::post('asignAgent/{id}', 'AgentController@asignAgent')->name('asignAgent');


//patient
Route::get('patient', 'HospitalController@allPatient')->name('patient');

Route::get('patient/profile/{id}', 'HospitalController@authPatient')->name('patient.profile');

//dashboard
Route::get('dashboard','AdminController@dashboard')->name('dashboard');
Route::post('dashboard/serach', 'AdminController@filterDate')->name('date.filter');
// Route::get('dashboard','AdminController@dashboard')->name('dashboard');
Route::get('dashboard_test', function () {
    return view('admin.dashboard');
});



//hospital Panel

//dashboard
Route::prefix('hospitalPanel')->group(function () {


    Route::get('notification', function () {
        return view('hospital.common.notification1');
    })->name('notification.hospital');

    Route::get('dashboard', 'Hospital_Panel\DashboardController@dashboard')->name('hosPanel.dashboard');
    Route::post('dashboard/serach', 'Hospital_Panel\DashboardController@search')->name('hosPanel.search');
    Route::get('profile', 'Hospital_Panel\ProfileController@hospitalProfile')->name('hosPanel.profile');

    Route::get('booking', 'Hospital_Panel\BookingController@authBooking')->name('booking.hospital');
    Route::get('booking/details/{id}', 'Hospital_Panel\BookingController@viewBooking')->name('hosPanel.booking.details');
    Route::get('booking/history/{id}', 'Hospital_Panel\BookingController@history')->name('hosPanel.booking.history');
    Route::get('booking/profile/{id}', 'Hospital_Panel\BookingController@authPatient')->name('hosPanel.patient.profile');


    Route::post('verifyOTP', 'Hospital_Panel\BookingController@verifyOTP')->name('otp.verification');

    Route::get('settings', function () {
        return view('hospital.settings.settings');
    })->name('settings.hospital');
});


// Route::get('hospital/dashboard', function () {
//     return view('hospital.dashboard.dashboard');
// })->name('hosPanel.dashboard');

// Route::get('hospital/settings', function () {
//     return view('hospital.settings.settings');
// })->name('settings.hospital');

});


// Route::get('/500', function () {
//     return view('errors.500');
//     // return response()->json(null, 401);
// });

// Route::get('/404', function () {
//     return view('errors.404');
//     // return response()->json(null, 401);
// });


// Route::middleware('auth:api')->group(function () {

//     Route::get('/dashboard', function () {
//         return view('purpel');
//     });
// });


//  Route::get('event', 'PayController@index');
//  Route::post('pay', 'PayController@pay');
//  Route::get('pay-success', 'PayController@success');


// Auth::routes();
Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@redirect')->name('home');
