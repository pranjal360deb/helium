<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDeatilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking', function (Blueprint $table) {
            //
            $table->string('agent_id')->nullable()->after('test_id');
            $table->string('booking_id')->nullable()->after('agent_id');
            $table->string('patient_category_id')->nullable()->after('booking_id');
            $table->string('dob')->nullable()->after('patient_category_id');
            $table->string('identity_proof',500)->nullable()->after('dob');
            $table->string('identity_proof_No',500)->nullable()->after('identity_proof');

            // $table->string('is_foreign_country',500)->nullable()->after('identity_proof_No');
            // $table->string('foreign_country_place',500)->nullable()->after('is_foreign_country');
            // $table->string('is_contact_with_lab',500)->nullable()->after('foreign_country_place');
            // $table->string('place_contact_lab',500)->nullable()->after('is_contact_with_lab');
            // $table->string('is_quarantined',500)->nullable()->after('place_contact_lab');
            // $table->string('where_quarantined',500)->nullable()->after('is_quarantined');
            // $table->string('is_cough',500)->default('No')->after('where_quarantined');
            // $table->string('is_vomit',500)->default('No')->after('is_cough');
            // $table->string('is_breathlessness',500)->default('No')->after('is_vomit');
            // $table->string('is_haemoptysis',500)->default('No')->after('is_breathlessness');
            // $table->string('is_soar_throat',500)->default('No')->after('is_haemoptysis');
            // $table->string('is_nasal_discharge',500)->default('No')->after('is_soar_throat');
            // $table->string('is_sputum',500)->default('No')->after('is_nasal_discharge');
            // $table->string('is_fever_at_evaluation',500)->default('No')->after('is_sputum');
            // $table->string('is_diarrhoea',500)->default('No')->after('is_fever_at_evaluation');
            // $table->string('is_chest_pain',500)->default('No')->after('is_diarrhoea');
            // $table->string('is_abdominal_pain',500)->default('No')->after('is_chest_pain');
            // $table->string('is_nausea',500)->default('No')->after('is_abdominal_pain');
            // $table->string('is_body_ache',500)->default('No')->after('is_nausea');

            // $table->string('is_COPD',500)->default('No')->after('is_body_ache');
            // $table->string('is_chronic_renal_disease',500)->default('No')->after('is_COPD');
            // $table->string('is_diabetes',500)->default('No')->after('is_chronic_renal_disease');
            // $table->string('is_heart_disease',500)->default('No')->after('is_diabetes');
            // $table->string('is_bronshitis',500)->default('No')->after('is_heart_disease');
            // $table->string('is_hypertension',500)->default('No')->after('is_bronshitis');
            // $table->string('is_malignancy',500)->default('No')->after('is_hypertension');
            // $table->string('is_asthma',500)->default('No')->after('is_malignancy');

            // $table->string('hospitalization_date',500)->nullable()->after('is_asthma');
            // $table->string('diagnosis',500)->nullable()->after('hospitalization_date');
            // $table->string('differential_diagnosis',500)->nullable()->after('diagnosis');
            // $table->string('etiology_identified',500)->nullable()->after('differential_diagnosis');
            // $table->string('phone_no',500)->nullable()->after('etiology_identified');
            // $table->string('hospital_name_address',500)->nullable()->after('phone_no');
            // $table->string('name_of_the_doctor',500)->nullable()->after('hospital_name_address');
            // $table->string('date',500)->nullable()->after('name_of_the_doctor');

            $table->text('report',500)->nullable()->after('identity_proof_No');










        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking', function (Blueprint $table) {
            //
        });
    }
}
