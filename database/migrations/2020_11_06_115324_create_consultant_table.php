<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConsultantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultant', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->bigInteger('role_id')->nullable();
            $table->string('email')->nullable();
            $table->string('phone_no')->nullable();
            $table->string('added_by')->nullable();
            $table->string('age')->nullable();
            $table->string('gender')->nullable();
            $table->text('address')->nullable();
            $table->string('pin')->nullable();
            $table->string('location',500)->nullable();
            $table->string('country_id')->nullable();
            $table->string('state_id')->nullable();
            $table->string('city_id')->nullable();
            $table->string('is_deactivate')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consultant');
    }
}
