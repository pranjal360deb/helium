<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_details', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('booking_id');
            $table->string('is_foreign_country',500)->nullable();
            $table->string('foreign_country_place',500)->nullable();
            $table->string('is_contact_with_lab',500)->nullable();
            $table->string('place_contact_lab',500)->nullable();
            $table->string('is_quarantined',500)->nullable();
            $table->string('where_quarantined',500)->nullable();
            $table->string('is_cough',500)->nullable();
            $table->string('is_vomit',500)->nullable();
            $table->string('is_breathlessness',500)->nullable();
            $table->string('is_haemoptysis',500)->nullable();
            $table->string('is_soar_throat',500)->nullable();
            $table->string('is_nasal_discharge',500)->nullable();
            $table->string('is_sputum',500)->nullable();
            $table->string('is_fever_at_evaluation',500)->nullable();
            $table->string('is_diarrhoea',500)->nullable();
            $table->string('is_chest_pain',500)->nullable();
            $table->string('is_abdominal_pain',500)->nullable();
            $table->string('is_nausea',500)->nullable();
            $table->string('is_body_ache',500)->nullable();

            // $table->string('is_COPD',500)->nullable();
            // $table->string('is_chronic_renal_disease',500)->nullable();
            // $table->string('is_diabetes',500)->nullable();
            // $table->string('is_heart_disease',500)->nullable();
            // $table->string('is_bronshitis',500)->nullable();
            // $table->string('is_hypertension',500)->nullable();
            // $table->string('is_malignancy',500)->nullable();
            // $table->string('is_asthma',500)->nullable();

            // $table->string('hospitalization_date',500)->nullable();
            // $table->string('diagnosis',500)->nullable();
            // $table->string('differential_diagnosis',500)->nullable();
            // $table->string('etiology_identified',500)->nullable();
            // $table->string('phone_no',500)->nullable();
            // $table->string('hospital_name_address',500)->nullable();
            // $table->string('name_of_the_doctor',500)->nullable();
            // $table->string('date',500)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_details');
    }
}
