<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPatientDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('booking', function (Blueprint $table) {
            //
             $table->string('is_COPD',500)->default('No')->after('report');
            $table->string('is_chronic_renal_disease',500)->default('No')->after('is_COPD');
            $table->string('is_diabetes',500)->default('No')->after('is_chronic_renal_disease');
            $table->string('is_heart_disease',500)->default('No')->after('is_diabetes');
            $table->string('is_bronshitis',500)->default('No')->after('is_heart_disease');
            $table->string('is_hypertension',500)->default('No')->after('is_bronshitis');
            $table->string('is_malignancy',500)->default('No')->after('is_hypertension');
            $table->string('is_asthma',500)->default('No')->after('is_malignancy');

            $table->string('hospitalization_date',500)->nullable()->after('is_asthma');
            $table->string('diagnosis',500)->nullable()->after('hospitalization_date');
            $table->string('differential_diagnosis',500)->nullable()->after('diagnosis');
            $table->string('etiology_identified',500)->nullable()->after('differential_diagnosis');
            $table->string('phone_no',500)->nullable()->after('etiology_identified');
            $table->string('hospital_name_address',500)->nullable()->after('phone_no');
            $table->string('name_of_the_doctor',500)->nullable()->after('hospital_name_address');
            $table->string('date',500)->nullable()->after('name_of_the_doctor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booking', function (Blueprint $table) {
            //
        });
    }
}
